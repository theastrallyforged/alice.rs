# alice.rs

[Alice](https://github.com/m-ender/alice) is a [Fungeoid](https://esolangs.org/wiki/Fungeoid) created by [Martin Ender](https://github.com/m-ender). It has eight directions instead of the usual four, mirrors that refract instead of reflecting, and a pervasive (though certainly not unpleasant) duality. Read [the language specification](https://github.com/m-ender/alice/blob/master/README.md) to learn more.

alice.rs is an unofficial implementation of Alice in Rust.

Source code, revision history, and issue tracking are available at <https://codeberg.org/theastrallyforged/alice.rs>. alice.rs releases are additionally published on `crates.io.`.

## Caveats Up Front

Building alice.rs from source requires Rust 1.43.0 (released 2020-04-23) or later. (Prebuilt binaries may be made available in the future.)

## License and Copyright

alice.rs is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

alice.rs is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

A copy of the GNU General Public License is distributed with alice.rs (the `LICENSE.md` file). You can also find it at <https://www.gnu.org/licenses/gpl-3.0.en.html>.

alice.rs is Copyright (C) 2020 The Astrally Forged <pgp: C9A371F28975220C8A3A05C9FE83103DD3F25055>.

alice.rs contains code that is derivative of Martin Ender's original implementation, which is Copyright (c) 2016 Martin Ender and licensed with the MIT license.
