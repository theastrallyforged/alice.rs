// Associated program (located at `repeat_nth_elements.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/145029 on 2017-10-11, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $original_string:expr, $count:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--divide-by-zero=exit")
                                .arg("--stuck=panic")
                                .arg("tests/repeat_nth_elements.alice")
                                .write_stdin(concat!(stringify!($count), "\n", $original_string))
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by caird coinheringaahing, published at
// https://codegolf.stackexchange.com/questions/145016/repeat-the-nth-elements/145029 on
// 2017-10-11, licensed CC BY-SA 3.0
test!(hello_world: "Hello, World!", 3 => "HHHellllo,   Worrrld!!!");
test!(code_golf: "Code golf", 1 => "Code golf");
test!(abcdefghijklm: "abcdefghijklm", 10 => "aaaaaaaaaabcdefghijkkkkkkkkkklm");
test!(capital_t: "tesTing", 6 => "ttttttesTingggggg");
test!(
    long:
    "very very very long string for you to really make sure that your program works",
    4
    => "vvvvery    veryyyy verrrry loooong sssstrinnnng foooor yoooou toooo reaaaally    makeeee surrrre thhhhat yyyyour    proggggram    workkkks"
);
