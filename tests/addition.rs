// Associated program (located at `addition.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/116094 on 2017-04-11, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident, $augend:expr, $addend:expr, $sum:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/addition.alice")
                                .write_stdin(stringify!($augend $addend))
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout(stringify!($sum));
        }
    };
}

// These test cases by dkudriavtsev, published at
// https://codegolf.stackexchange.com/questions/84260/add-two-numbers on 2016-07-02,
// licensed CC BY-SA 3.0
test!(add_1_2, 1, 2, 3);
test!(add_14_15, 14, 15, 29);
test!(add_7_9, 7, 9, 16);
test!(add_e1_8, -1, 8, 7);
test!(add_8_e9, 8, -9, -1);
test!(add_e8_e9, -8, -9, -17);
