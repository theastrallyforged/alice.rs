// Associated program (located at `others_sum_parity.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/118536 on 2017-05-02, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/others_sum_parity.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Leaky Nun, published at
// https://codegolf.stackexchange.com/questions/118505/parity-of-sum-of-other-elements on
// 2017-05-02, licensed CC BY-SA 3.0
test!(a_1_2_3_1: "1, 2, 3, 1" => "0\n1\n0\n0\n");
test!(a_1_2_3_2_1: "1, 2, 3, 2, 1" => "0\n1\n0\n1\n0\n");
test!(a_2_2: "2, 2" => "0\n0\n");
test!(a_100_1001: "100, 1001" => "1\n0\n");
