// Associated program (located at `interleaving_reversing.alice`) by Leo, published at
// https://codegolf.stackexchange.com/a/123081 on 2017-05-27, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/interleaving_reversing.alice")
                                .write_stdin(concat!($input, "\n"))
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout(concat!($output, "\n"));
        }
    };
}

// These test cases by Erik the Outgolfer, published at
// https://codegolf.stackexchange.com/questions/123070/interleaving-reversing on
// 2017-05-27, licensed CC BY-SA 3.0
test!(ai: "abcdefghi" => "ahcfedgbi");
test!(aj: "abcdefghij" => "ajchefgdib");
test!(hello_world: "Hello, World!" => "HdlroW ,olle!");
test!(hello_no_comma: "Hello World!" => "H!llooW rlde");
test!(ascii_uppercase: "ABCDEFGHIJKLMNOPQRSTUVWXYZ" => "AZCXEVGTIRKPMNOLQJSHUFWDYB");
test!(ascii_printable_plus_blank:
    r##" !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~"##
    => r##" }"{$y&w(u*s,q.o0m2k4i6g8e:c<a>_@]B[DYFWHUJSLQNOPMRKTIVGXEZC\A^?`=b;d9f7h5j3l1n/p-r+t)v'x%z#|!~"##
);
test!(p: "P" => "P");
test!(ab: "AB" => "AB");
test!(xyz: "xyz" => "xyz");
