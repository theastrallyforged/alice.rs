// Associated program (located at `string_waves.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/126966 on 2017-06-16, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/string_waves.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Mr. Xcoder, published at
// https://codegolf.stackexchange.com/questions/126935/make-string-waves on 2017-06-16, licensed CC
// BY-SA 3.0
test!(empty: "" => "");
test!(hello_world: "Hello World" => "WeH");
test!(waves: "Waves" => "aW");
test!(instance_of_fallen: "Programming Puzzles and Code Golf" => "GoCauPorP");
test!(yay: "Yay Got it" => "iGY");
test!(gratitude: "Thx for the feedback" => "eeftf");
test!(overshort_passphrase: "Go Cat Print Pad" => "PPCG");
test!(i_scream: "ICE CREAM" => "RCCI");
