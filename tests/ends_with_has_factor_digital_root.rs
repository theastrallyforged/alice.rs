// Associated program (located at `ends_with_has_factor_digital_root.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/149205 on 2017-11-27, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/ends_with_has_factor_digital_root.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by user72269, published at
// https://codegolf.stackexchange.com/questions/149196/find-the-smallest-positive-integer-which-ends-in-n-is-divisible-by-n-and-whose
// on 2017-11-27, licensed CC BY-SA 3.0
test!(n_12: "12" => "912");
test!(n_13: "13" => "11713");
test!(n_14: "14" => "6314");
test!(n_15: "15" => "915");
test!(n_16: "16" => "3616");
test!(n_17: "17" => "15317");
test!(n_18: "18" => "918");
test!(n_19: "19" => "17119");
test!(n_20: "20" => "9920");
test!(n_40: "40" => "1999840");
// Don't test `n = 100`: it takes several minutes on its own; ain't nocreature got time for that.
