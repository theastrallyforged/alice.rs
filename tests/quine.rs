// Associated program (located at `quine.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/116278 on 2017-04-12, licensed CC BY-SA 3.0

#[test]
fn quine() {
    assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                        .unwrap()
                        .arg("--stuck=panic")
                        .arg("tests/quine.alice")
                        .timeout(std::time::Duration::from_secs(10))
                        .assert()
                        .success()
                        .stdout(include_str!("quine.alice"));
}
