// Associated program (located at `microwave_timer_input.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/147817 on 2017-11-11, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/microwave_timer_input.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Andrew Brēza, published at
// https://codegolf.stackexchange.com/questions/147777/how-long-should-my-microwave-run on
// 2017-11-11, licensed CC BY-SA 3.0
test!(low_digits: "1234" => "754");
test!(high_digits: "9876" => "5956");
test!(one_and_a_half: "130" => "90");
test!(three_halves: "90" => "90");
test!(ones_1: "1" => "1");
test!(ones_2: "11" => "11");
test!(ones_3: "111" => "71");
test!(ones_4: "1111" => "671");
test!(nines_1: "9" => "9");
test!(nines_2: "99" => "99");
test!(nines_3: "999" => "639");
test!(nines_4: "9999" => "6039");
