// Associated program (located at `vibrating_phone.alice`) by Nitrodon, published at
// https://codegolf.stackexchange.com/a/140542 on 2017-08-26, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/vibrating_phone.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by tisaconundrum, published at
// https://codegolf.stackexchange.com/questions/140430/pick-up-your-phone-its-vibrating on
// 2017-08-25, licensed CC BY-SA 3.0
test!(llsls: "long long short long short" => "Rrrr - Rrrr - Rr - Rrrr - Rr");
test!(lllsss: "long long long short short short" => "Rrrr - Rrrr - Rrrr - Rr - Rr - Rr");
test!(sss_sss: "short short short pause short short short" => "Rr - Rr - Rr - - - Rr - Rr - Rr");
test!(lssll_ss: "long short short long long pause short short" => "Rrrr - Rr - Rr - Rrrr - Rrrr - - - Rr - Rr");
