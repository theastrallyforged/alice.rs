// Associated program (located at `subfactorial.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/148981 on 2017-11-24, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/subfactorial.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Martin Ender, published at
// https://codegolf.stackexchange.com/questions/113936/deranged-combinatorics-compute-the-subfactorial
// on 2017-03-26, licensed CC BY-SA 3.0
test!(n_0: "0" => "1");
test!(n_1: "1" => "0");
test!(n_2: "2" => "1");
test!(n_3: "3" => "2");
test!(n_4: "4" => "9");
test!(n_5: "5" => "44");
test!(n_6: "6" => "265");
test!(n_10: "10" => "1334961");
test!(n_12: "12" => "176214841");
test!(n_13: "13" => "2290792932");
test!(n_14: "14" => "32071101049");
test!(n_20: "20" => "895014631192902121");
test!(n_21: "21" => "18795307255050944540");
test!(
    n_100:
    "100"
    => "34332795984163804765195977526776142032365783805375784983543400282685180793327632432791396429850988990237345920155783984828001486412574060553756854137069878601"
);
