// Associated program (located at `dennis_appreciation.alice`) by Nitrodon, published at
// https://codegolf.stackexchange.com/a/123226 on 2017-05-28, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/dennis_appreciation.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by HyperNeutrino, published at
// https://codegolf.stackexchange.com/questions/123194/user-appreciation-challenge-1-dennis on
// 2017-05-28, licensed CC BY-SA 3.0
test!(dennis: "Dennis" => "1");
test!(martin: "Martin" => "1");
test!(martin_ender: "Martin Ender" => "1");
test!(alex: "Alex" => "");
test!(alex_a: "Alex A." => "1");
test!(doorknob: "Doorknob" => "");
test!(mego: "Mego" => "");
