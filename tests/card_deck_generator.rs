// Associated program (located at `card_deck_generator.alice`) by Nitrodon, published at
// https://codegolf.stackexchange.com/a/124808 on 2017-06-06, licensed CC BY-SA 3.0

const EXPECTED_OUTPUT_SORTED: [char; 52] = [
    '🂡', '🂢', '🂣', '🂤', '🂥', '🂦', '🂧', '🂨', '🂩', '🂪', '🂫', '🂭', '🂮',
    '🂱', '🂲', '🂳', '🂴', '🂵', '🂶', '🂷', '🂸', '🂹', '🂺', '🂻', '🂽', '🂾',
    '🃁', '🃂', '🃃', '🃄', '🃅', '🃆', '🃇', '🃈', '🃉', '🃊', '🃋', '🃍', '🃎',
    '🃑', '🃒', '🃓', '🃔', '🃕', '🃖', '🃗', '🃘', '🃙', '🃚', '🃛', '🃝', '🃞',
];

#[test]
fn card_deck_generator() {
    let output = std::process::Command::new(concat!("target/debug/", env!("CARGO_PKG_NAME")))
                                       .arg("--stuck=panic")
                                       .arg("tests/card_deck_generator.alice")
                                       .output()
                                       .unwrap();
    let mut output_chars: Vec<_> = String::from_utf8(output.stdout).unwrap().chars().collect();
    assert_ne!(&*output_chars, &EXPECTED_OUTPUT_SORTED[..]);
    output_chars.sort_unstable();
    assert_eq!(&*output_chars, &EXPECTED_OUTPUT_SORTED[..]);
    assert!(output.status.success());
}
