// Associated program (located at `smallest_int_smaller_code.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/116242 on 2017-04-12, licensed CC BY-SA 3.0

#[test]
fn smallest_int_smaller_code() {
    assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                        .unwrap()
                        .arg("--stuck=panic")
                        .arg("tests/smallest_int_smaller_code.alice")
                        .timeout(std::time::Duration::from_secs(10))
                        .assert()
                        .success()
                        .stdout("3628800");
}
