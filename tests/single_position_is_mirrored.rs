// Associated program (located at `single_position_is_mirrored.alice`) by Nitrodon, published at
// https://codegolf.stackexchange.com/a/123896 on 2017-06-01, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $string:expr, $index:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/single_position_is_mirrored.alice")
                                .write_stdin(concat!($string, "\n", stringify!($index), "\n"))
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Stephen, published at
// https://codegolf.stackexchange.com/questions/123833/does-the-nth-char-equal-the-nth-from-last-char
// on 2017-05-31, licensed CC BY-SA 3.0
test!(n_1: "1", 1 => "Jabberwocky");
test!(abc: "abc", 2 => "Jabberwocky");
test!(aaaaaaa: "aaaaaaa", 4 => "Jabberwocky");
test!(bracket_pair_1: "[][]", 2 => "");
test!(bracket_pair_0: "[][]", 1 => "");
test!(ppqqpq: "ppqqpq", 3 => "Jabberwocky");
test!(ababab: "ababab", 6 => "");
test!(n_12345: "12345", 1 => "");
test!(the_word_letter: "letter", 2 => "Jabberwocky");
test!(zxywv: "zxywv", 4 => "");
