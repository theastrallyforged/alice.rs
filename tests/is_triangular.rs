// Associated program (located at `is_triangular.alice`) by Luis Mendo, published at
// https://codegolf.stackexchange.com/a/122107 on 2017-05-22, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/is_triangular.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by ETHproductions, published at
// https://codegolf.stackexchange.com/questions/122087/is-this-number-triangular on 2017-05-22,
// licensed CC BY-SA 3.0
test!(n_1: "1" => "1");
test!(n_3: "3" => "1");
test!(n_6: "6" => "1");
test!(n_10: "10" => "1");
test!(n_15: "15" => "1");
test!(n_21: "21" => "1");
test!(n_55: "55" => "1");
test!(n_276: "276" => "1");
test!(n_1540: "1540" => "1");
test!(n_2701: "2701" => "1");
test!(n_5050: "5050" => "1");
test!(n_7626: "7626" => "1");
test!(n_18915: "18915" => "1");
test!(n_71253: "71253" => "1");
test!(n_173166: "173166" => "1");
test!(n_222111: "222111" => "1");
test!(n_303031: "303031" => "1");
test!(n_307720: "307720" => "1");
test!(n_500500: "500500" => "1");
test!(n_998991: "998991" => "1");
test!(n_2: "2" => "0");
test!(n_4: "4" => "0");
test!(n_5: "5" => "0");
test!(n_7: "7" => "0");
test!(n_8: "8" => "0");
test!(n_9: "9" => "0");
test!(n_11: "11" => "0");
test!(n_16: "16" => "0");
test!(n_32: "32" => "0");
test!(n_50: "50" => "0");
test!(n_290: "290" => "0");
test!(n_555: "555" => "0");
test!(n_4576: "4576" => "0");
test!(n_31988: "31988" => "0");
test!(n_187394: "187394" => "0");
test!(n_501500: "501500" => "0");
test!(n_999999: "999999" => "0");
