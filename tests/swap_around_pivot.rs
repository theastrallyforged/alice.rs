// Associated program (located at `swap_around_pivot.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/150196 on 2017-12-08, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $main:expr, $pivot:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/swap_around_pivot.alice")
                                .write_stdin(concat!($main, "\n", $pivot, "\n"))
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by ThePlasmaRailgun, published at
// https://codegolf.stackexchange.com/questions/150168/reverse-two-sections-of-a-string-around-a-pivot
// on 2017-12-08, licensed CC BY-SA 3.0
test!(number_word_range_and_back: "OneTwoThreeTwoOne", "Two" => "ThreeTwoOneTwoOne");
test!(digit_range: "1Two2Two3Two4", "Two" => "2Two3Two4Two1");
test!(number_word_range: "OneTwoThree", "Two" => "ThreeTwoOne");
test!(fabled_race: "the rabbit is faster than the turtle", " is faster than " => "the turtle is faster than the rabbit");
test!(digit_range_hyphen_separator: "1-2-3-4-5-6", "-" => "2-3-4-5-6-1");
