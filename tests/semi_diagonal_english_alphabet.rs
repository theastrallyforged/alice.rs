// Associated program (located at `semi_diagonal_english_alphabet.alice`) by Nitrodon, published at
// https://codegolf.stackexchange.com/a/140429 on 2017-08-25, licensed CC BY-SA 3.0

const MAX_EXPECTED_OUTPUT: &str = "\
A
 B B
  C C C
   D D D D
    E E E E E
     F F F F F F
      G G G G G G G
       H H H H H H H H
        I I I I I I I I I
         J J J J J J J J J J
          K K K K K K K K K K K
           L L L L L L L L L L L L
            M M M M M M M M M M M M M
             N N N N N N N N N N N N N N
              O O O O O O O O O O O O O O O
               P P P P P P P P P P P P P P P P
                Q Q Q Q Q Q Q Q Q Q Q Q Q Q Q Q Q
                 R R R R R R R R R R R R R R R R R R
                  S S S S S S S S S S S S S S S S S S S
                   T T T T T T T T T T T T T T T T T T T T
                    U U U U U U U U U U U U U U U U U U U U U
                     V V V V V V V V V V V V V V V V V V V V V V
                      W W W W W W W W W W W W W W W W W W W W W W W
                       X X X X X X X X X X X X X X X X X X X X X X X X
                        Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y
                         Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z
";

#[test]
fn semi_diagonal_english_alphabet() {
    for (letter, line_count) in (b'A'..=b'Z').zip(1..) {
        let expected_output = {
            let lines: Vec<_> = MAX_EXPECTED_OUTPUT.lines()
                                                   .take(line_count)
                                                   .map(|s| format!("{}\n", s))
                                                   .collect();
            lines.join("")
        };
        assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                            .unwrap()
                            .arg("--stuck=panic")
                            .arg("tests/semi_diagonal_english_alphabet.alice")
                            .write_stdin(format!("{}", char::from(letter)))
                            .timeout(std::time::Duration::from_secs(10))
                            .assert()
                            .success()
                            .stdout(expected_output);
    }
}
