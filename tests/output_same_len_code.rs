// Associated program (located at `output_same_len_code.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/121174 on 2017-05-17, licensed CC BY-SA 3.0

#[test]
fn output_same_len_code() {
    assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                        .unwrap()
                        .arg("--stuck=panic")
                        .arg("tests/output_same_len_code.alice")
                        .timeout(std::time::Duration::from_secs(10))
                        .assert()
                        .success()
                        .stdout("103\ng");
}
