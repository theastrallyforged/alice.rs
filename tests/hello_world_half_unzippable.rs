// Associated program (located at `hello_world_half_unzippable.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/128569 on 2017-06-27, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg(concat!("tests/", stringify!($name), ".alice"))
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout("Hello, World!");
        }
    };
}

test!(hello_world_half_unzippable);
test!(hello_world_half_unzipped);
