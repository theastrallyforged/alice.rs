// Associated program (located at `reverse_quine.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/116279 on 2017-04-12, licensed CC BY-SA 3.0

#[test]
fn reverse_quine() {
    let reversed: String = include_str!("reverse_quine.alice").chars().rev().collect();
    assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                        .unwrap()
                        .arg("--stuck=panic")
                        .arg("tests/reverse_quine.alice")
                        .timeout(std::time::Duration::from_secs(10))
                        .assert()
                        .success()
                        .stdout(reversed);
}
