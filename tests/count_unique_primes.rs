// Associated program (located at `count_unique_primes.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/144650 on 2017-10-08, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/count_unique_primes.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Gryphon, published at
// https://codegolf.stackexchange.com/questions/144633/how-many-unique-primes on 2017-10-08,
// licensed CC BY-SA 3.0
test!(n_24: "24" => "2");
test!(n_126: "126" => "3");
test!(n_1538493: "1538493" => "4");
test!(n_123456: "123456" => "3");

#[test]
fn oeis() {
    // This is A001221 from the Online Encyclopedia of Integer Sequences
    // ( https://oeis.org/A001221 ), licensed CC BY-NC 3.0
    const EXPECTED_OUTPUTS: &[u16] = &[
        0, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 2, 1, 2, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 1, 2, 1, 2,
        1, 3, 1, 1, 2, 2, 2, 2, 1, 2, 2, 2, 1, 3, 1, 2, 2, 2, 1, 2, 1, 2, 2, 2, 1, 2, 2, 2,
        2, 2, 1, 3, 1, 2, 2, 1, 2, 3, 1, 2, 2, 3, 1, 2, 1, 2, 2, 2, 2, 3, 1, 2, 1, 2, 1, 3,
        2, 2, 2, 2, 1, 3, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 1, 3, 1, 2, 3, 2, 1, 2, 1, 3, 2,
    ];

    for (input, output) in (1..).zip(EXPECTED_OUTPUTS) {
        assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                            .unwrap()
                            .arg("tests/count_unique_primes.alice")
                            .write_stdin(format!("{}", input))
                            .timeout(std::time::Duration::from_secs(10))
                            .assert()
                            .success()
                            .stdout(format!("{}", output));
    }
}
