// Associated program (located at `prime_ant.alice`) by Nitrodon, published at
// https://codegolf.stackexchange.com/a/144710 on 2017-10-09, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/prime_ant.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Arnaud, published at
// https://codegolf.stackexchange.com/questions/144695/the-prime-ant on 2017-10-09, licensed
// CC BY-SA 3.0
test!(zero: "0" => "0");
test!(ten: "10" => "6");
test!(forty_seven: "47" => "9");
test!(four_seven_three_four: "4734" => "274");
test!(ten_thousand: "10000" => "512");
