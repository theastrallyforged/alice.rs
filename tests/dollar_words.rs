// Associated program (located at `dollar_words.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/116972 on 2017-04-19, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/dollar_words.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Stephen, published at
// https://codegolf.stackexchange.com/questions/116933/bernardino-identifies-unaltered-dollar-words
// on 2017-04-18, licensed CC BY-SA 3.0
test!(buzzy: "buzzy" => "1");
test!(boycott: "boycott" => "1");
test!(identifies: "identifies" => "1");
test!(adiabatically: "adiabatically" => "1");
test!(ttttt: "ttttt" => "1");
test!(zzz: "zzz" => "0");
test!(zzzzzzz: "zzzzzzz" => "0");
test!(abcdefghiljjjzz: "abcdefghiljjjzz" => "0");
test!(tttt: "tttt" => "0");
test!(basic: "basic" => "0");
