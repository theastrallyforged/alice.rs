// Associated program (located at `odd_letter_out.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/149327 on 2017-11-28, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/odd_letter_out.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by juniorRubyist, published at
// https://codegolf.stackexchange.com/questions/149316/oddem-out-letters on 2017-11-28, licensed
// CC BY-SA 3.0
test!(
    b_10_5_d:
    "\
        bbbbbbbbbb\
        bbbbbdbbbb\
        bbbbbbbbbb\
        bbbbbbbbbb\
        bbbbbbbbbb\
    "
    => "d"
);
test!(
    a_7_5_i:
    "\
        AAAAAAA\
        AAAAAAA\
        AAAAAAA\
        AAAIAAA\
        AAAAAAA\
    "
    => "I"
);
test!(
    v_10_5_q:
    "\
        vvqvvvvvvv\
        vvvvvvvvvv\
        vvvvvvvvvv\
        vvvvvvvvvv\
        vvvvvvvvvv\
    "
    => "q"
);
test!(
    p_9_10_u:
    "\
        puuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
        uuuuuuuuu\
    "
    => "p"
);
