#[test]
fn repeat_escape() {
    assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                        .unwrap()
                        .arg("--stuck=panic")
                        .arg("tests/repeat_escape.alice")
                        .timeout(std::time::Duration::from_secs(10))
                        .assert()
                        .success()
                        .stdout("aaaa\n");
}
