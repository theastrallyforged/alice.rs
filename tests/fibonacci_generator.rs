// Associated program (located at `fibonacci_generator.alice`) by Martin Ender, from Alice
// reference implementation, published at
// https://github.com/m-ender/alice/blob/master/examples/fibonacci-loop.alice, licensed MIT

use std::io::Read;

const EXPECTED_OUTPUT: &[u8] = include_bytes!("fibonacci_sequence");

#[test]
fn fibonacci_generator() {
    let process = std::process::Command::new(concat!("target/debug/", env!("CARGO_PKG_NAME")))
                                        .arg("--stuck=panic")
                                        .arg("tests/fibonacci_generator.alice")
                                        .stdin(std::process::Stdio::null())
                                        .stdout(std::process::Stdio::piped())
                                        .spawn()
                                        .unwrap();
    let mut output_buffer = vec![0; EXPECTED_OUTPUT.len()];
    process.stdout.unwrap().read_exact(&mut output_buffer).unwrap();
    assert_eq!(output_buffer, EXPECTED_OUTPUT);
}
