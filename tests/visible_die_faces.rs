// Associated program (located at `visible_die_faces.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/149914 on 2017-12-04, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/visible_die_faces.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by FlipTack, published at
// https://codegolf.stackexchange.com/questions/149890/visible-dice-faces on 2017-12-04,
// licensed CC BY-SA 3.0
test!(sides_6: "6" => "Jabberwocky");
test!(sides_62: "62" => "Jabberwocky");
test!(sides_13: "13" => "Jabberwocky");
test!(sides_213: "213" => "Jabberwocky");
test!(sides_326: "326" => "Jabberwocky");
test!(sides_16: "16" => "");
test!(sides_542: "542" => "");
test!(sides_314: "314" => "");
test!(sides_5462: "5462" => "");
test!(sides_123456: "123456" => "");
