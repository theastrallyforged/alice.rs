// Associated program (located at `string_reverse_ish.alice`) by Martin Ender, published at
// https://codegolf.stackexchange.com/a/144550 on 2017-10-06, licensed CC BY-SA 3.0

macro_rules! test {
    ( $name:ident: $input:expr => $output:expr ) => {
        #[test]
        fn $name() {
            assert_cmd::Command::cargo_bin(env!("CARGO_PKG_NAME"))
                                .unwrap()
                                .arg("--stuck=panic")
                                .arg("tests/string_reverse_ish.alice")
                                .write_stdin($input)
                                .timeout(std::time::Duration::from_secs(10))
                                .assert()
                                .success()
                                .stdout($output);
        }
    };
}

// These test cases by Comrade SparklePony, published at
// https://codegolf.stackexchange.com/questions/144544/reverse-ish-a-string on 2017-10-06, licensed
// CC BY-SA 3.0
test!(hello_world: "Hello, World!" => "!dlloW ,olleH");
test!(number_18464399: "18464399" => "99343488");
test!(code_golf: "Code Golf" => "floG eloC");
test!(agag: "abcdefgABCDEFG" => "GFEDCBAgfedcba");
test!(mmm_marshmallows: "Mmm, marshmallows" => "swwllwmhsrwm  mms");
test!(number_15147: "15147" => "74751");
