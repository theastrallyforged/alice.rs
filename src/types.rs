use std::{fmt, iter};
use std::cmp::{max, min};
use std::convert::TryFrom;

use rug::Assign;

use crate::char_try_from_int;

use self::Direction::*;
use self::Mode::*;

#[derive(Debug, Clone, Default)]
pub struct Grid {
    tiles: std::collections::HashMap<(isize, isize), Tile>,
    min_x: isize,
    min_y: isize,
    max_x: isize,
    max_y: isize,
}

impl Grid {
    const TILE_AXIS_LEN: usize = 16;

    pub fn width(&self) -> usize {
        let difference = self.max_x.wrapping_sub(self.min_x) as usize;
        if let Some(width) = difference.checked_add(1) {
            width
        } else {
            panic!("integer overflow: grid too large")
        }
    }

    pub fn height(&self) -> usize {
        let difference = self.max_y.wrapping_sub(self.min_y) as usize;
        if let Some(height) = difference.checked_add(1) {
            height
        } else {
            panic!("integer overflow: grid too large")
        }
    }

    pub fn get(&self, coordinates: (isize, isize)) -> Option<&Int> {
        let (m, (n_x, n_y)) = Self::coordinates_to_indices(coordinates);
        self.tiles.get(&m).map(|t| &t[n_y][n_x])
    }

    pub fn put<T>(&mut self, coordinates: (isize, isize), value: T) -> impl Sized + '_ where Int: Assign<T> {
        let cell = self.get_mut_or_insert_default(coordinates);
        let was_set = *cell != -1;
        cell.assign(value);
        let is_set = *cell != -1;
        match (was_set, is_set) {
            (false, true) => self.expand_boundaries_to(coordinates),
            (true, false) => self.did_clear_cell(coordinates),
            (false, false) | (true, true) => {},
        }
    }

    pub(crate) fn put_axis(&mut self, mut pointer: GridPointer, string: &[char]) {
        if string.is_empty() {
            return
        }
        self.expand_boundaries_to(pointer.position);
        for &char_ in string {
            self.get_mut_or_insert_default(pointer.position).assign(u32::from(char_));
            pointer.tick();
        }
        self.expand_boundaries_to(pointer.next_position_in(pointer.direction.turn_around()));
        // We don't need to call `did_clear_cell()` because `char`s can't be `-1`.
    }

    fn get_mut_or_insert_default(&mut self, coordinates: (isize, isize)) -> &mut Int {
        let (m, (n_x, n_y)) = Self::coordinates_to_indices(coordinates);
        &mut self.tiles.entry(m).or_insert_with(Self::new_tile)[n_y][n_x]
    }

    pub fn find_label(&self, label: &[char], direction: Direction) -> Option<(isize, isize)> {
        for mut axis_iter in self.scan_typed(direction) {
            while axis_iter.has_next() {
                let label_found = axis_iter.by_ref()
                                           .take(label.len())
                                           .map(char_try_from_int)
                                           .eq(label.iter().copied().map(Some));
                if label_found {
                    return Some(axis_iter.pointer.next_position_in(direction.turn_around()))
                }
            }
        }
        None
    }

    pub fn scan(&self, direction: Direction) -> impl iter::Iterator<Item=impl iter::Iterator<Item=&'_ Int>> {
        self.scan_typed(direction)
    }

    fn scan_typed(&self, direction: Direction) -> GridScanner<'_> {
        GridScanner {
            grid: self,
            minor_direction: direction,
            pointer: GridPointer {
                direction: direction.turn_right(),
                position: match direction {
                    North => (self.min_x, self.max_y),
                    Northeast => (self.min_x, self.min_y),
                    East => (self.min_x, self.min_y),
                    Southeast => (self.max_x, self.min_y),
                    South => (self.max_x, self.min_y),
                    Southwest => (self.max_x, self.max_y),
                    West => (self.max_x, self.max_y),
                    Northwest => (self.min_x, self.max_y),
                },
            },
        }
    }

    fn expand_boundaries_to(&mut self, (x, y): (isize, isize)) {
        self.min_x = min(x, self.min_x);
        self.max_x = max(x, self.max_x);
        self.min_y = min(y, self.min_y);
        self.max_y = max(y, self.max_y);
    }

    /// Handles the removal of a cell by recomputing the grid boundaries
    fn did_clear_cell(&mut self, (x, y): (isize, isize)) {
        if x > self.min_x && x < self.max_x && y > self.min_y && y < self.max_y {
            return
        }
        let (m, (n_x, n_y)) = Self::coordinates_to_indices((x, y));
        if let Some(tile) = self.tiles.get(&m) {
            let (mut x_edge_preserved, mut y_edge_preserved) = (false, false);
            for cell in &tile[n_y] {
                x_edge_preserved |= *cell != -1;
            }
            for cell in tile.iter().map(|a| &a[n_x]) {
                y_edge_preserved |= *cell != -1;
            }
            if x_edge_preserved && y_edge_preserved {
                return
            }
        }
        let [mut min_x, mut max_x, mut min_y, mut max_y] = [0; 4];
        self.tiles.retain(|&m, t| {
            let mut empty = true;
            for (n_y, axis) in t.iter().enumerate() {
                for (n_x, cell) in axis.iter().enumerate() {
                    if *cell != -1 {
                        empty = false;
                        let (x, y) = Self::indices_to_coordinates((m, (n_x, n_y)));
                        min_x = min(x, min_x);
                        max_x = max(x, max_x);
                        min_y = min(y, min_y);
                        max_y = max(y, max_y);
                    }
                }
            }
            !empty
        });
        (self.min_x = min_x, self.max_x = max_x, self.min_y = min_y, self.max_y = max_y);
    }

    fn new_tile() -> Tile {
        array_init::array_init(|_| array_init::array_init(|_| Int::from(-1)))
    }

    fn coordinates_to_indices((x, y): (isize, isize)) -> ((isize, isize), (usize, usize)) {
        let axis_len = Self::TILE_AXIS_LEN as isize;
        (
            (x.div_euclid(axis_len), y.div_euclid(axis_len)),
            (x.rem_euclid(axis_len) as usize, y.rem_euclid(axis_len) as usize),
        )
    }

    fn indices_to_coordinates(((m_x, m_y), (n_x, n_y)): ((isize, isize), (usize, usize))) -> (isize, isize) {
        let axis_len = Self::TILE_AXIS_LEN as isize;
        (m_x * axis_len + n_x as isize, m_y * axis_len + n_y as isize)
    }
}

type Tile = [[Int; Grid::TILE_AXIS_LEN]; Grid::TILE_AXIS_LEN];

#[derive(Debug, Clone)]
struct GridScanner<'g> {
    grid: &'g Grid,
    pointer: GridPointer,
    minor_direction: Direction,
}

impl<'g> iter::Iterator for GridScanner<'g> {
    type Item = GridAxisScanner<'g>;

    fn next(&mut self) -> Option<Self::Item> {
        self.grid.get(self.pointer.position)?;
        let sub = GridAxisScanner {
            grid: self.grid,
            pointer: GridPointer { direction: self.minor_direction, .. self.pointer },
        };
        self.pointer.position = self.pointer.next_position();
        let is_in_bounds = |d| self.grid.get(self.pointer.next_position_in(d)).is_some();
        if is_in_bounds(self.minor_direction.turn_around()) {
            self.pointer.position = self.pointer.next_position_in(self.minor_direction.turn_around());
        } else if self.grid.get(self.pointer.position).is_some() {
            self.pointer.position = self.pointer.position;
        } else if is_in_bounds(self.minor_direction) {
            self.pointer.position = self.pointer.next_position_in(self.minor_direction);
        }
        Some(sub)
    }
}

impl iter::FusedIterator for GridScanner<'_> {}

#[derive(Debug, Clone)]
pub struct GridAxisScanner<'g> {
    grid: &'g Grid,
    pointer: GridPointer,
}

impl GridAxisScanner<'_> {
    pub fn position(&self) -> (isize, isize) {
        self.pointer.position
    }

    fn has_next(&self) -> bool {
        self.grid.get(self.pointer.position).is_some()
    }
}

impl<'g> iter::Iterator for GridAxisScanner<'g> {
    type Item = &'g Int;

    fn next(&mut self) -> Option<Self::Item> {
        let item = self.grid.get(self.pointer.position)?;
        self.pointer.position = self.pointer.next_position();
        Some(item)
    }
}

impl iter::FusedIterator for GridAxisScanner<'_> {}

#[derive(Debug, Clone, Default)]
pub struct Stack {
    inner: Vec<StackItem>,
}

impl Stack {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Stack { inner: Vec::with_capacity(capacity) }
    }

    pub fn push(&mut self, value: impl Into<StackItem>) {
        self.inner.push(value.into())
    }

    // Not `impl IntoIterator`, because we'd have to commit to a specific concrete iterator type,
    // and Moonbolt doesn't want to do that (at least not yet).
    pub(crate) fn into_iter(self) -> impl iter::Iterator<Item=StackItem> + DoubleEndedIterator {
        self.inner.into_iter()
    }

    pub fn push_cardinal(&mut self, value: impl Into<Int>) {
        self.push(value.into())
    }

    pub fn push_ordinal(&mut self, value: impl Into<String>) {
        self.push(value.into())
    }

    pub fn pop_cardinal(&mut self) -> Int {
        match self.inner.pop() {
            Some(StackItem::Cardinal(number)) => number,
            Some(StackItem::Ordinal(string)) => {
                let mut chars = string.into_iter().peekable();
                let mut maybe_number = None;
                loop {
                    match chars.next() {
                        Some('-') => {
                            if let Some(number) = maybe_number.take() {
                                self.push(number);
                            } else {
                                if let Some(char_ @ '0'..='9') = chars.peek().copied() {
                                    drop(chars.next());
                                    let small_int = -(char_ as i8 - b'0' as i8);
                                    maybe_number = Some(small_int.into());
                                }
                            }
                        },
                        Some(char_ @ '0'..='9') => {
                            let digit = char_ as i8 - b'0' as i8;
                            let number = maybe_number.get_or_insert_with(Int::new);
                            *number *= 10;
                            *number += if *number >= 0 { digit } else { -digit };
                        },
                        Some(_) => if let Some(number) = maybe_number.take() {
                            self.push(number);
                        },
                        None => {
                            if let Some(number) = maybe_number.take() {
                                self.push(number);
                            }
                            break
                        },
                    }
                }
                self.pop_cardinal()
            },
            None => 0.into(),
        }
    }

    pub fn pop_ordinal(&mut self) -> String {
        match self.inner.pop() {
            Some(StackItem::Cardinal(number)) => format!("{}", number).chars().collect(),
            Some(StackItem::Ordinal(string)) => string,
            None => Vec::new(),
        }
    }
}

impl std::ops::Deref for Stack {
    type Target = Vec<StackItem>;

    fn deref(&self) -> &Vec<StackItem> {
        &self.inner
    }
}

impl std::ops::DerefMut for Stack {
    fn deref_mut(&mut self) -> &mut Vec<StackItem> {
        &mut self.inner
    }
}

impl<T: Into<StackItem>> iter::FromIterator<T> for Stack {
    fn from_iter<I: IntoIterator<Item=T>>(iterable: I) -> Self {
        Stack { inner: iterable.into_iter().map(T::into).collect() }
    }
}

impl<T: Into<StackItem>> Extend<T> for Stack {
    fn extend<I: IntoIterator<Item=T>>(&mut self, iterable: I) {
        self.inner.extend(iterable.into_iter().map(T::into))
    }
}

#[derive(Debug, Clone, Default)]
pub struct Tape {
    inner: std::collections::VecDeque<Int>,
    offset: isize,
}

impl Tape {
    pub fn get(&self, position: isize) -> Option<&Int> {
        self.inner.get(self.position_to_index(position))
    }

    pub fn get_word(&self, position: isize) -> impl iter::Iterator<Item=char> + '_ {
        self.inner
            .iter()
            .skip(self.position_to_index(position))
            .scan((), |(), n| char_try_from_int(n))
    }

    pub fn put<T>(&mut self, position: isize, value: T) -> impl Sized + '_ where Int: Assign<T>, T: Into<Int> {
        if position > 0 {
            self.expand_to(position - 1);
        } else if position < 0 {
            self.expand_to(position + 1);
        }
        match self.inner.get_mut(self.position_to_index(position)) {
            Some(cell) => cell.assign(value),
            None if position < 0 => {
                self.inner.push_front(value.into());
                self.offset += 1;
            },
            None => self.inner.push_back(value.into()),
        }
    }

    pub fn put_word(&mut self, position: isize, string: &[char]) -> impl Sized + '_ {
        if string.is_empty() {
            self.expand_to(position);
            let index = self.position_to_index(position);
            self.inner[index].assign(-1);
            return
        }
        self.expand_to(position + string.len() as isize - 1);
        let start_index = self.position_to_index(position);
        let mut cells = self.inner.iter_mut().skip(start_index);
        for (cell, &char_) in cells.by_ref().zip(string) {
            cell.assign(u32::from(char_));
        }
        if let Some(cell) = cells.next() {
            cell.assign(-1);
        } else {
            // Nothing to do: the cell isn't materialized, so it's implicitly `-1`.
        }
    }

    pub(crate) fn search(&self, needle: &Int, mut position: isize, direction: TapeDirection) -> Option<isize> {
        loop {
            match direction {
                TapeDirection::Reverse => position = position.checked_sub(1)?,
                TapeDirection::Forward => position = position.checked_add(1)?,
            }
            match self.get(position) {
                Some(number) if number == needle => return Some(position),
                None if *needle == -1 => return Some(position),
                Some(_) => {},
                None => return None,
            }
        }
    }

    pub(crate) fn search_word(&self, subneedle: &[char], mut position: isize, direction: TapeDirection) -> Option<isize> {
        loop {
            match direction {
                TapeDirection::Reverse => position = self.rewind_word(position)?,
                TapeDirection::Forward => position = self.forward_word(position)?,
            }
            let mut chars = self.get_word(position).peekable();
            while chars.peek().is_some() {
                if chars.by_ref().eq(subneedle.iter().copied()) {
                    return Some(position)
                }
            }
        }
    }

    pub(crate) fn rewind_word(&self, position: isize) -> Option<isize> {
        let mut index = self.position_to_index(position);
        let char_to_the_left = |i: usize| {
            self.inner
                .get(i.checked_sub(1)?)
                .map_or(Some(false), |n| Some(char_try_from_int(n).is_some()))
        };
        while char_to_the_left(index)? {
            index = index.checked_sub(1)?;
        }
        index = index.checked_sub(1)?;
        while char_to_the_left(index)? {
            index = index.checked_sub(1)?;
        }
        Some(self.index_to_position(index))
    }

    pub(crate) fn forward_word(&self, position: isize) -> Option<isize> {
        let mut index = self.position_to_index(position);
        let char_here = |i| self.inner
                                .get(i)
                                .map_or(false, |n| char_try_from_int(n).is_some());
        while char_here(index) {
            index = index.checked_add(1)?;
        }
        index = index.checked_add(1)?;
        Some(self.index_to_position(index))
    }

    pub(crate) fn join_all(&self) -> impl iter::Iterator<Item=char> + '_ {
        self.inner.iter().filter_map(char_try_from_int)
    }

    fn expand_to(&mut self, new_edge: isize) {
        let filler_factory = || Int::from(-1);
        if new_edge >= self.inner.len() as isize - self.offset {
            let new_len = new_edge.saturating_add(self.offset).saturating_add(1);
            self.inner.resize_with(new_len as usize, filler_factory);
        } else if new_edge < 0 {
            let new_item_count = max(-new_edge - self.offset, 0) as usize;
            self.inner.extend(std::iter::repeat_with(filler_factory).take(new_item_count));
            self.inner.rotate_right(new_item_count);
            self.offset += new_item_count as isize;
        }
    }

    fn position_to_index(&self, position: isize) -> usize {
        (position + self.offset) as usize
    }

    fn index_to_position(&self, index: usize) -> isize {
        index as isize - self.offset
    }
}

#[derive(Clone)]
pub enum StackItem {
    Cardinal(Int),
    Ordinal(String),
}

impl From<Int> for StackItem {
    fn from(int: Int) -> Self {
        Self::Cardinal(int)
    }
}

impl From<String> for StackItem {
    fn from(string: String) -> Self {
        Self::Ordinal(string)
    }
}

impl From<i32> for StackItem {
    fn from(int: i32) -> Self {
        Int::from(int).into()
    }
}

impl From<char> for StackItem {
    fn from(char_: char) -> Self {
        vec![char_].into()
    }
}

impl From<Option<char>> for StackItem {
    fn from(maybe_char: Option<char>) -> Self {
        match maybe_char {
            Some(char_) => char_.into(),
            None => String::new().into(),
        }
    }
}

impl fmt::Debug for StackItem {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            StackItem::Cardinal(number) => number.fmt(formatter),
            StackItem::Ordinal(string) => {
                write!(formatter, "{}", "\"")?;
                for &char_ in string {
                    write!(formatter, "{}", char_.escape_debug())?;
                }
                write!(formatter, "{}", "\"")?;
                Ok(())
            },
        }
    }
}

impl IntoIterator for StackItem {
    type IntoIter = Iterator;
    type Item = Option<char>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            StackItem::Cardinal(count) => Iterator::Cardinal { count },
            StackItem::Ordinal(string) => Iterator::Ordinal(string.into_iter()),
        }
    }
}

#[derive(Debug, Clone)]
pub enum Iterator {
    Cardinal { count: Int },
    Ordinal(std::vec::IntoIter<char>),
}

impl Default for Iterator {
    fn default() -> Self {
        Iterator::Cardinal { count: 1.into() }
    }
}

impl iter::Iterator for Iterator {
    type Item = Option<char>;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Iterator::Cardinal { count } if *count <= 0 => None,
            Iterator::Cardinal { count } => {
                *count -= 1;
                Some(None)
            },
            Iterator::Ordinal(iter) => iter.next().map(Some),
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        match self {
            Iterator::Cardinal { count } => match usize::try_from(count) {
                Ok(count) => (count, Some(count)),
                Err(_) => (usize::MAX, None)
            },
            Iterator::Ordinal(iter) => iter.size_hint(),
        }
    }
}

impl iter::FusedIterator for Iterator {}

impl iter::DoubleEndedIterator for Iterator {
    fn next_back(&mut self) -> Option<Self::Item> {
        match self {
            Iterator::Cardinal { .. } => self.next(),
            Iterator::Ordinal(iter) => iter.next_back().map(Some),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Ip {
    pub position: (isize, isize),
    pub direction: Direction,
    pub(crate) stuck_action: crate::StuckAction,
}

impl Ip {
    pub fn mode(&self) -> Mode {
        self.direction.mode()
    }

    pub fn tick(&mut self, grid: &Grid) {
        let mut new_position = self.next_position();
        match self.direction {
            North => {
                if new_position.1 < grid.min_y {
                    new_position.1 = grid.max_y
                }
            },
            East => {
                if new_position.0 > grid.max_x {
                    new_position.0 = grid.min_x
                }
            },
            South => {
                if new_position.1 > grid.max_y {
                    new_position.1 = grid.min_y
                }
            },
            West => {
                if new_position.0 < grid.min_x {
                    new_position.0 = grid.max_x
                }
            },
            Northeast | Southeast | Southwest | Northwest => {
                if grid.width() <= 1 || grid.height() <= 1 {
                    return
                }
                if !(grid.min_x..=grid.max_x).contains(&new_position.0) {
                    self.direction = self.direction.reflect_horizontally();
                }
                if !(grid.min_y..=grid.max_y).contains(&new_position.1) {
                    self.direction = self.direction.reflect_vertically();
                }
                new_position = self.next_position();
            },
        }
        self.position = new_position;
        if !(grid.min_x..=grid.max_x).contains(&self.position.0) {
            self.handle_stuck()
        }
        if !(grid.min_y..=grid.max_y).contains(&self.position.1) {
            self.handle_stuck()
        }
    }

    pub(crate) fn next_position(&self) -> (isize, isize) {
        let Ip { position, direction, .. } = *self;
        let grid_pointer = GridPointer { position, direction };
        grid_pointer.next_position()
    }

    pub(crate) fn handle_stuck(&self) -> ! {
        match self.stuck_action {
            crate::StuckAction::Panic => {
                let ip = self;
                dbg!(ip);
                panic!("instruction pointer is stuck out of bounds")
            },
            crate::StuckAction::Hang => loop {
                std::thread::sleep(std::time::Duration::from_secs(u64::MAX));
            },
        }
    }
}

#[derive(Debug, Clone)]
pub struct GridPointer {
    pub position: (isize, isize),
    pub direction: Direction,
}

impl GridPointer {
    pub fn tick(&mut self) {
        self.position = self.next_position();
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn next_position(&self) -> (isize, isize) {
        self.next_position_in(self.direction)
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn next_position_in(&self, direction: Direction) -> (isize, isize) {
        let (x_increment, y_increment) = direction.to_increments();
        let (x, y) = self.position;
        if let (Some(x), Some(y)) = (x.checked_add(x_increment), y.checked_add(y_increment)) {
            (x, y)
        } else {
            let position = self.position;
            dbg!(position, direction, direction.to_increments());
            panic!("grid pointer movement resulted in integer overflow")
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) enum TapeHeads {
    Separate {
        cardinal: isize,
        ordinal: isize,
    },
    Shared(isize),
}

impl TapeHeads {
    pub(crate) fn new(initial_position: isize, separate: bool) -> Self {
        if separate {
            TapeHeads::Separate {
                cardinal: initial_position,
                ordinal: initial_position,
            }
        } else {
            TapeHeads::Shared(initial_position)
        }
    }

    pub(crate) fn cardinal(&mut self) -> &mut isize {
        match self {
            TapeHeads::Separate { cardinal, .. } => cardinal,
            TapeHeads::Shared(shared) => shared,
        }
    }

    pub(crate) fn ordinal(&mut self) -> &mut isize {
        match self {
            TapeHeads::Separate { ordinal, .. } => ordinal,
            TapeHeads::Shared(shared) => shared,
        }
    }
}

pub type Int = rug::Integer;

pub type String = Vec<char>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Direction {
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest,
}

impl Direction {
    pub fn mode(self) -> Mode {
        match self {
            North | East | South | West => Cardinal,
            Northeast | Southeast | Southwest | Northwest => Ordinal,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn refract_forwards(self) -> Self {
        match self {
            North => Southwest,
            Northeast => South,
            East => Southeast,
            Southeast => East,
            South => Northeast,
            Southwest => North,
            West => Northwest,
            Northwest => West,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn refract_backwards(self) -> Self {
        match self {
            North => Southeast,
            Northeast => East,
            East => Northeast,
            Southeast => North,
            South => Northwest,
            Southwest => West,
            West => Southwest,
            Northwest => South,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn reflect_horizontally(self) -> Self {
        match self {
            North => North,
            Northeast => Northwest,
            East => West,
            Southeast => Southwest,
            South => South,
            Southwest => Southeast,
            West => East,
            Northwest => Northeast,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn reflect_vertically(self) -> Self {
        match self {
            North => South,
            Northeast => Southeast,
            East => East,
            Southeast => Northeast,
            South => North,
            Southwest => Northwest,
            West => West,
            Northwest => Southwest,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn half_reflect_northwards(self) -> Self {
        match self {
            North | South => North,
            Northeast | Southeast => Northeast,
            East => East,
            West => West,
            Northwest | Southwest => Northwest,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn half_reflect_eastwards(self) -> Self {
        match self {
            North => North,
            Northeast | Northwest => Northeast,
            East | West => East,
            Southeast | Southwest => Southeast,
            South => South,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn half_reflect_southwards(self) -> Self {
        match self {
            North | South => South,
            Northeast | Southeast => Southeast,
            East => East,
            Southwest | Northwest => Southwest,
            West => West,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn half_reflect_westwards(self) -> Self {
        match self {
            North => North,
            Northeast | Northwest => Northwest,
            East | West => West,
            Southeast | Southwest => Southwest,
            South => South,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn turn_left(self) -> Self {
        match self {
            North => West,
            Northeast => Northwest,
            East => North,
            Southeast => Northeast,
            South => East,
            Southwest => Southeast,
            West => South,
            Northwest => Southwest,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn turn_right(self) -> Self {
        match self {
            North => East,
            Northeast => Southeast,
            East => South,
            Southeast => Southwest,
            South => West,
            Southwest => Northwest,
            West => North,
            Northwest => Northeast,
        }
    }

    #[must_use = "this returns the result of the operation, without modifying the original"]
    pub fn turn_around(self) -> Self {
        match self {
            North => South,
            Northeast => Southwest,
            East => West,
            Southeast => Northwest,
            South => North,
            Southwest => Northeast,
            West => East,
            Northwest => Southeast,
        }
    }

    pub fn to_increments(self) -> (isize, isize) {
        match self {
            North => (0, -1),
            Northeast => (1, -1),
            East => (1, 0),
            Southeast => (1, 1),
            South => (0, 1),
            Southwest => (-1, 1),
            West => (-1, 0),
            Northwest => (-1, -1),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Mode {
    Cardinal,
    Ordinal,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TapeDirection {
    Reverse,
    Forward,
}
