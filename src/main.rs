#![recursion_limit="256"]

use std::cmp::{Ordering, max, min};
use std::collections::{HashMap, HashSet, VecDeque};
use std::convert::TryFrom;
use std::fmt::Write as _;
use std::io::{Read, Write};
use std::iter::FromIterator;
use std::mem::swap;

use rand::Rng;
use rug::Assign;

pub mod io;
pub mod types;

use crate::io::ReadExt;
use crate::types::{Grid, GridPointer, Int, Ip, Stack, StackItem, String, Tape};
use crate::types::Direction::*;
use crate::types::Mode::*;

macro_rules! dispatch_instruction {
    ( @internal { $iterators:expr, $stack:expr } { $loop_label:lifetime, $scrutinee:expr } { $($finished_arms:tt)* } {} ) => {
        match $scrutinee { $($finished_arms)* }
    };

    ( @internal { $iterators:expr, $stack:expr } { $loop_label:lifetime, $scrutinee:expr } { $($finished_arms:tt)* } { $($pats:pat)| + => noiter $expr:expr, $($rest:tt)* } ) => {
        dispatch_instruction! {
            @internal
            { $iterators, $stack }
            { $loop_label, $scrutinee }
            { $($finished_arms)* $($pats)| + => $expr, }
            { $($rest)* }
        }
    };

    ( @internal { $iterators:expr, $stack:expr } { $loop_label:lifetime, $scrutinee:expr } { $($finished_arms:tt)* } { $($pats:pat)| + => $expr:expr, $($rest:tt)* } ) => {
        dispatch_instruction! {
            @internal
            { $iterators, $stack }
            { $loop_label, $scrutinee }
            {
                $($finished_arms)*
                $($pats)| + => {
                    let iterator = $iterators.pop_front()
                                             .map(StackItem::into_iter)
                                             .unwrap_or_default();
                    #[allow(unused_labels)]
                    $loop_label: for item in iterator {
                        if let Some(char_) = item {
                            $stack.push(char_);
                        }
                        $expr;
                    }
                },
            }
            { $($rest)* }
        }
    };

    ( with { $iterators:expr, $stack:expr } $loop_label:lifetime: match { $scrutinee:expr } { $($arms:tt)* } ) => {
        dispatch_instruction! {
            @internal
            { $iterators, $stack }
            { $loop_label, $scrutinee }
            {}
            { $($arms)* }
        }
    };
}

fn main() {
    let (source_path, options, mut inner_args) = args();

    let mut grid = {
        let mut grid = Grid::default();
        let mut pointer = GridPointer { position: (0, 0), direction: East };
        let mut source_file = std::io::BufReader::new(std::fs::File::open(source_path).unwrap());
        let (mut longest_line_len, mut line_len): (isize, isize) = (0, 0);
        while let Some(char_) = source_file.read_char_skipping().unwrap() {
            if char_ != '\n' {
                grid.put(pointer.position, u32::from(char_));
                line_len += 1;
                pointer.tick();
            } else {
                pointer.position.1 += 1;
                pointer.position.0 = 0;
                longest_line_len = max(line_len, longest_line_len);
                line_len = 0;
            }
        }
        longest_line_len = max(line_len, longest_line_len);
        if options.ignore_trailing_newline {
            if pointer.position.0 <= 0 {
                pointer.position.1 -= 1;
            }
        }
        pointer.direction = West;
        while pointer.position.1 >= 0 {
            pointer.position.0 = longest_line_len.saturating_sub(1);
            while pointer.position.0 >= 0 && grid.get(pointer.position).map_or(true, |n| *n < 0) {
                grid.put(pointer.position, b' ');
                pointer.tick();
            }
            pointer.position.1 -= 1;
        }
        if grid.width() == 0 || grid.height() == 0 {
            grid.put((0, 0), b' ');
        }
        grid
    };

    let mut ip = Ip { position: (-1, 0), direction: East, stuck_action: options.stuck };
    let mut stack = Stack::default();
    let mut tape = Tape::default();
    let mut tape_heads = types::TapeHeads::new(0, options.separate_tape_heads);
    let mut returns = Vec::new();
    let mut iterators = VecDeque::new();
    let mut string_mode: Option<Vec<Int>> = None;

    let stdin_shared = std::io::stdin();
    let mut stdin = stdin_shared.lock();
    let stdout_shared = std::io::stdout();
    let mut stdout = stdout_shared.lock();
    let mut rng = rand::thread_rng();

    'main_loop: loop {
        ip.tick(&grid);
        let (instruction, instruction_char);
        {
            let maybe_instruction = grid.get(ip.position).and_then(|n| u8::try_from(n).ok());
            instruction = maybe_instruction.unwrap_or(0xfd);
            instruction_char = maybe_instruction.map(char::from).unwrap_or('\u{fffd}');
        }
        let mode = ip.mode();

        dispatch_instruction!(
            with { iterators, stack } 'instruction_iteration: match { (instruction, mode, &mut string_mode) } {
                // Escaping (overrides everything)
                (b'\'', _, _) => noiter {
                    ip.tick(&grid);
                    let iterator = iterators.pop_front()
                                            .map(StackItem::into_iter)
                                            .unwrap_or_default();
                    for item in iterator {
                        if let Some(char_) = item {
                            stack.push(char_);
                        }
                        match (mode, &mut string_mode) {
                            (Cardinal, None) => {
                                let next_value = grid.get(ip.position)
                                                     .cloned()
                                                     .unwrap_or_else(|| Int::from(-1));
                                stack.push(next_value);
                            },
                            (Ordinal, None) => {
                                if let Some(char_) = grid.get(ip.position).and_then(char_try_from_int) {
                                    stack.push(char_);
                                } else {
                                    stack.push(String::new());
                                }
                            },
                            (_, Some(string)) => {
                                let next_value = grid.get(ip.position)
                                                     .cloned()
                                                     .unwrap_or_else(|| Int::from(-1));
                                string.push(next_value);
                            },
                        }
                    }
                },

                // Mirrors and walls (override string mode)
                (b'/', _, _) => noiter ip.direction = ip.direction.refract_forwards(),
                (b'\\', _, _) => noiter ip.direction = ip.direction.refract_backwards(),
                (b'_', _, _) => noiter ip.direction = ip.direction.reflect_vertically(),
                (b'|', _, _) => noiter ip.direction = ip.direction.reflect_horizontally(),

                // String mode
                (b'"', _, None) => noiter string_mode = Some(Vec::new()),
                (b'"', _, Some(_)) => noiter {
                    enum String {
                        Cardinal(Vec<Int>),
                        Ordinal(types::String),
                    }
                    let string = string_mode.take().unwrap();
                    let string = match mode {
                        Cardinal => String::Cardinal(string),
                        Ordinal => String::Ordinal(string.iter().filter_map(char_try_from_int).collect()),
                    };
                    let iterator = iterators.pop_front()
                                            .map(StackItem::into_iter)
                                            .unwrap_or_default();
                    {
                        let (lower_bound, maybe_upper_bound) = iterator.size_hint();
                        let size_hint = maybe_upper_bound.unwrap_or(lower_bound);
                        let string_len = match &string {
                            String::Cardinal(ints) => ints.len(),
                            String::Ordinal(_) => 1,
                        };
                        stack.reserve(size_hint.saturating_mul(string_len));
                    }
                    for item in iterator {
                        if let Some(char_) = item {
                            stack.push(char_);
                        }
                        match &string {
                            String::Cardinal(ints) => stack.extend(ints.iter().cloned()),
                            String::Ordinal(string) => stack.push(string.clone()),
                        }
                    }
                },
                (_, _, Some(string)) => noiter string.push(grid.get(ip.position).cloned().unwrap_or_else(|| Int::from(-1))),

                // Debugging
                (b'`', _, None) => noiter {
                    eprintln!();
                    dbg!(&ip, mode, &grid, &stack, &tape, &tape_heads, &returns, &iterators, &string_mode, inner_args.as_slice());
                    eprintln!();
                },

                // Control flow
                (b'@', _, None) => break 'main_loop,
                (b'<', Cardinal, None) => ip.direction = West,
                (b'>', Cardinal, None) => ip.direction = East,
                (b'^', Cardinal, None) => ip.direction = North,
                (b'v', Cardinal, None) => ip.direction = South,
                (b'<', Ordinal, None) => ip.direction = ip.direction.half_reflect_westwards(),
                (b'>', Ordinal, None) => ip.direction = ip.direction.half_reflect_eastwards(),
                (b'^', Ordinal, None) => ip.direction = ip.direction.half_reflect_northwards(),
                (b'v', Ordinal, None) => ip.direction = ip.direction.half_reflect_southwards(),
                (b'{', _, None) => ip.direction = ip.direction.turn_left(),
                (b'}', _, None) => ip.direction = ip.direction.turn_right(),
                (b'=', mode, None) => {
                    let ordering = match mode {
                        Cardinal => {
                            let n = stack.pop_cardinal();
                            n.cmp0()
                        },
                        Ordinal => {
                            let b = stack.pop_ordinal();
                            let a = stack.pop_ordinal();
                            a.cmp(&b)
                        },
                    };
                    match ordering {
                        Ordering::Less => ip.direction = ip.direction.turn_left(),
                        Ordering::Greater => ip.direction = ip.direction.turn_right(),
                        Ordering::Equal => {},
                    }
                },
                (b'#', Cardinal, None) => iterators.push_front(0.into()),
                (b'#', Ordinal, None) => iterators.push_front(String::new().into()),
                (b'$', Cardinal, None) => {
                    let n = stack.pop_cardinal();
                    if n == 0 {
                        iterators.push_front(0.into());
                    }
                },
                (b'$', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    if s.is_empty() {
                        iterators.push_front(String::new().into());
                    }
                },
                (b'j', Cardinal, None)
                | (b'J', Cardinal, None)
                => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    if let (Ok(x), Ok(y)) = (isize::try_from(&x), isize::try_from(&y)) {
                        if instruction.is_ascii_lowercase() {
                            returns.push(ip.position);
                        }
                        ip.position = (x, y);
                    } else {
                        dbg!(instruction_char, mode, x, y);
                        panic!("jump instruction resulted in integer overflow in instruction pointer position")
                    }
                },
                (b'j', Ordinal, None)
                | (b'J', Ordinal, None)
                => {
                    let s = stack.pop_ordinal();
                    if let Some(address) = grid.find_label(&s, ip.direction) {
                        if instruction.is_ascii_lowercase() {
                            returns.push(ip.position);
                        }
                        ip.position = address;
                    }
                },
                (b'k', _, None)
                | (b'K', _, None)
                => {
                    let maybe_address = if instruction.is_ascii_lowercase() {
                        returns.pop()
                    } else {
                        returns.last().copied()
                    };
                    if let Some(address) = maybe_address {
                        ip.position = address;
                    }
                },
                (b'w', _, None) => returns.push(ip.position),
                (b'W', _, None) => drop(returns.pop()),
                (b'&', Cardinal, None) => iterators.push_back(stack.pop_cardinal().into()),
                (b'&', Ordinal, None) => iterators.push_back(stack.pop_ordinal().into()),

                // Constants
                (b'0'..=b'9', Cardinal, None) => stack.push(Int::from(instruction - b'0')),
                (b'0'..=b'9', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    s.push(instruction_char);
                    stack.push(s);
                },
                (b'a', Cardinal, None) => stack.push(Int::from(10)),
                (b'a', Ordinal, None) => stack.push('\n'),
                (b'e', Cardinal, None) => stack.push(Int::from(-1)),
                (b'e', Ordinal, None) => stack.push(String::new()),

                // Input and output
                (b'i', Cardinal, None) => {
                    let mut byte = 0;
                    if stdin.read(std::slice::from_mut(&mut byte)).unwrap() > 0 {
                        stack.push(Int::from(byte))
                    } else {
                        stack.push(Int::from(-1))
                    }
                },
                (b'i', Ordinal, None) => stack.push(stdin.read_chars_to_end().unwrap()),
                (b'I', Cardinal, None) => match stdin.read_char_skipping().unwrap() {
                    Some(char_) => stack.push(Int::from(u32::from(char_))),
                    None => stack.push(Int::from(-1)),
                },
                (b'I', Ordinal, None) => stack.push(stdin.read_chars_line().unwrap()),
                (b'o', Cardinal, None) => {
                    let byte = stack.pop_cardinal().to_u8_wrapping();
                    stdout.write_all(std::slice::from_ref(&byte)).unwrap();
                },
                (b'O', Cardinal, None) => {
                    if let Some(char_) = char_try_from_int(&stack.pop_cardinal()) {
                        write!(stdout, "{}", char_).unwrap();
                    }
                },
                (b'o', Ordinal, None)
                | (b'O', Ordinal, None)
                => {
                    let mut stdout_buffered = std::io::BufWriter::new(&mut stdout);
                    let s = stack.pop_ordinal();
                    for char_ in s {
                        write!(stdout_buffered, "{}", char_).unwrap();
                    }
                    if instruction.is_ascii_uppercase() {
                        write!(stdout_buffered, "\n").unwrap();
                    }
                    stdout_buffered.flush().unwrap();
                },
                (b'M', Cardinal, None) => stack.push_cardinal(inner_args.len()),
                (b'M', Ordinal, None) => stack.push(inner_args.next().unwrap_or(String::new())),

                // Grid manipulation
                (b'g', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    let cell = match (isize::try_from(&x), isize::try_from(&y)) {
                        (Ok(x), Ok(y)) => grid.get((x, y)),
                        _ => None,
                    };
                    stack.push(cell.cloned().unwrap_or_else(|| Int::from(-1)));
                },
                (b'g', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let direction = ip.direction;
                    if let Some(position) = grid.find_label(&s, direction) {
                        let mut pointer = GridPointer { position, direction };
                        let iter = std::iter::from_fn(|| {
                            pointer.tick();
                            grid.get(pointer.position)
                        });
                        let string: String = iter.scan((), |(), n| char_try_from_int(n)).collect();
                        stack.push(string);
                    }
                },
                (b'p', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    let v = stack.pop_cardinal();
                    match (isize::try_from(&x), isize::try_from(&y)) {
                        (Ok(x), Ok(y)) => drop(grid.put((x, y), v)),
                        _ => {
                            dbg!(instruction_char, mode, v, x, y);
                            panic!("put-cell instruction resulted in integer overflow in grid coordinates")
                        },
                    }
                },
                (b'p', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let v = stack.pop_ordinal();
                    let direction = ip.direction;
                    if let Some(position) = grid.find_label(&s, direction) {
                        let pointer = GridPointer { position, direction };
                        grid.put_axis(pointer, &v);
                    }
                },

                // Stack manipulation
                (b',', Cardinal, None) => {
                    let n = stack.pop_cardinal();
                    match n.cmp0() {
                        Ordering::Greater => {
                            let offset = n.to_usize().unwrap_or(usize::MAX).saturating_add(1);
                            let value = if let Some(index) = stack.len().checked_sub(offset) {
                                stack.remove(index)
                            } else {
                                Int::from(0).into()
                            };
                            stack.push(value);
                        },
                        Ordering::Less => {
                            let filler_factory = || StackItem::Cardinal(Int::from(0));
                            let filler_len = {
                                let mut n = -n;
                                n -= &*rug::integer::SmallInteger::from(stack.len());
                                if n < 0 {
                                    n.assign(0);
                                }
                                // Just saturate. If we do end up getting `usize::MAX` here,
                                // `value` will make one more item, causing another overflow, and
                                // `Vec` will take care of generating an error and crashing.
                                n.to_usize().unwrap_or(usize::MAX)
                            };
                            let value = stack.pop().unwrap_or_else(filler_factory);
                            let filler = std::iter::repeat_with(filler_factory).take(filler_len);
                            stack.splice(0..0, std::iter::once(value).chain(filler));
                        },
                        Ordering::Equal => {},
                    }
                },
                (b',', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let filler = std::iter::repeat(StackItem::Ordinal(String::new()))
                                           .take(s.len().saturating_sub(stack.len()));
                    stack.splice(0..0, filler);
                    let range = stack.len().saturating_sub(s.len()) ..;
                    let mut permuting_part: Vec<_> = stack.drain(range.clone()).map(Some).collect();
                    let mut indices: Vec<_> = (0..permuting_part.len()).collect();
                    indices.sort_by_key(|&i| s[i]);
                    for index in indices {
                        stack.push(permuting_part[index].take().unwrap());
                    }
                },
                (b'~', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    stack.push(y);
                    stack.push(x);
                },
                (b'~', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    stack.push(b);
                    stack.push(a);
                },
                (b'.', Cardinal, None) => {
                    let n = stack.pop_cardinal();
                    stack.push(n.clone());
                    stack.push(n);
                },
                (b'.', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    stack.push(s.clone());
                    stack.push(s);
                },
                (b';', Cardinal, None) => drop(stack.pop_cardinal()),
                (b';', Ordinal, None) => drop(stack.pop_ordinal()),
                (b'Q', Cardinal, None) => noiter {
                    let n = {
                        let mut n = Int::from(0);
                        let iterator = iterators.pop_front()
                                                .map(StackItem::into_iter)
                                                .unwrap_or_default();
                        for item in iterator {
                            if let Some(char_) = item {
                                stack.push(char_);
                            }
                            n = max(n, stack.pop_cardinal());
                        }
                        n.to_usize().unwrap_or(usize::MAX)
                    };
                    let mut auxiliary_stack = Stack::with_capacity(n);
                    for _ in 0..n {
                        auxiliary_stack.push(stack.pop_cardinal());
                    }
                    stack.extend(auxiliary_stack.into_iter().rev());
                },
                (b'Q', Ordinal, None) => {
                    stack.reverse();
                    let mut buffer = std::string::String::new();
                    for v in stack.iter_mut() {
                        *v = match &*v {
                            s @ StackItem::Ordinal(_) => s.clone(),
                            StackItem::Cardinal(number) => {
                                buffer.clear();
                                write!(buffer, "{}", number).unwrap();
                                StackItem::Ordinal(buffer.chars().collect())
                            },
                        }
                    }
                },
                (b'd', Cardinal, None) => stack.push_cardinal(stack.len()),
                (b'd', Ordinal, None) => {
                    let mut concatenation = String::new();
                    let mut buffer = std::string::String::new();
                    for v in stack.iter() {
                        match v {
                            StackItem::Ordinal(string) => concatenation.extend(string.iter().copied()),
                            StackItem::Cardinal(number) => {
                                buffer.clear();
                                write!(buffer, "{}", number).unwrap();
                                concatenation.extend(buffer.chars());
                            },
                        }
                    }
                    stack.push(concatenation);
                },

                // Tape manipulation
                (b'!', Cardinal, None) => drop(tape.put(*tape_heads.cardinal(), stack.pop_cardinal())),
                (b'!', Ordinal, None) => drop(tape.put_word(*tape_heads.ordinal(), &stack.pop_ordinal())),
                (b'?', Cardinal, None) => stack.push(tape.get(*tape_heads.cardinal()).cloned().unwrap_or_else(|| Int::from(-1))),
                (b'?', Ordinal, None) => stack.push(String::from_iter(tape.get_word(*tape_heads.ordinal()))),
                (b'[', Cardinal, None) => *tape_heads.cardinal() = match tape_heads.cardinal().checked_sub(1) {
                    Some(head) => head,
                    None => {
                        dbg!(instruction_char, mode, tape_heads.cardinal());
                        panic!("rewind instruction resulted in integer overflow in Cardinal tape head")
                    },
                },
                (b'[', Ordinal, None) => *tape_heads.ordinal() = match tape.rewind_word(*tape_heads.ordinal()) {
                    Some(head) => head,
                    None => {
                        dbg!(instruction_char, mode, tape_heads.ordinal());
                        panic!("rewind instruction resulted in integer overflow in Ordinal tape head")
                    },
                },
                (b']', Cardinal, None) => *tape_heads.cardinal() = match tape_heads.cardinal().checked_add(1) {
                    Some(head) => head,
                    None => {
                        dbg!(instruction_char, mode, tape_heads.cardinal());
                        panic!("fast-forward instruction resulted in integer overflow in Cardinal tape head")
                    },
                },
                (b']', Ordinal, None) => *tape_heads.ordinal() = match tape.forward_word(*tape_heads.ordinal()) {
                    Some(head) => head,
                    None => {
                        dbg!(instruction_char, mode, tape_heads.ordinal());
                        panic!("fast-forward instruction resulted in integer overflow in Ordinal tape head")
                    },
                },
                (b'(', Cardinal, None)
                | (b')', Cardinal, None)
                => {
                    let n = stack.pop_cardinal();
                    let direction = match instruction {
                        b'(' => types::TapeDirection::Reverse,
                        b')' => types::TapeDirection::Forward,
                        _ => unreachable!(),
                    };
                    if let Some(head) = tape.search(&n, *tape_heads.cardinal(), direction) {
                        *tape_heads.cardinal() = head;
                    }
                },
                (b'(', Ordinal, None)
                | (b')', Ordinal, None)
                => {
                    let s = stack.pop_ordinal();
                    let direction = match instruction {
                        b'(' => types::TapeDirection::Reverse,
                        b')' => types::TapeDirection::Forward,
                        _ => unreachable!(),
                    };
                    if let Some(head) = tape.search_word(&s, *tape_heads.ordinal(), direction) {
                        *tape_heads.ordinal() = head;
                    }
                },
                (b'q', Cardinal, None) => stack.push(Int::from(*tape_heads.cardinal())),
                (b'q', Ordinal, None) => stack.push(String::from_iter(tape.join_all())),

                // Basic arithmetic and string operations
                (b'+', Cardinal, None)
                | (b'-', Cardinal, None)
                | (b'*', Cardinal, None)
                | (b':', Cardinal, None)
                | (b'%', Cardinal, None)
                => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    if (instruction == b':' || instruction == b'%') && y == 0 {
                        match options.divide_by_zero {
                            DivideByZeroAction::Panic => {
                                dbg!(instruction_char, mode, x, y);
                                panic!("arithmetic instruction resulted in division by zero")
                            },
                            DivideByZeroAction::Exit => break 'main_loop,
                        }
                    }
                    let output = match instruction {
                        b'+' => x + y,
                        b'-' => x - y,
                        b'*' => x * y,
                        b':' => rug::ops::DivRounding::div_floor(x, y),
                        b'%' => rug::ops::RemRounding::rem_floor(x, y),
                        _ => unreachable!(),
                    };
                    stack.push(output);
                },
                (b'+', Ordinal, None) => {
                    let mut strings = [stack.pop_ordinal(), stack.pop_ordinal()];
                    strings.sort_by_key(String::len);
                    let [shorter, mut longer] = strings;
                    for (char_, other_char) in longer.iter_mut().zip(&shorter) {
                        if u32::from(*other_char) > u32::from(*char_) {
                            *char_ = *other_char;
                        }
                    }
                    stack.push(longer);
                },
                (b'-', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let mut a = stack.pop_ordinal();
                    if b.is_empty() {
                        stack.push(a);
                        continue 'instruction_iteration
                    }
                    let mut position = a.len();
                    loop {
                        let mut iter = a[..min(position, a.len())].iter().enumerate().rev();
                        match iter.find(|(_, c)| *c == b.last().unwrap()) {
                            Some((index, _)) => {
                                position = index;
                                let needle_iter = b.iter().rev().skip(1);
                                let haystack_iter = iter.by_ref()
                                                        .map(|(_, c)| c)
                                                        .take(needle_iter.len());
                                if needle_iter.eq(haystack_iter) {
                                    a.splice(position+1-b.len() .. position+1, String::new());
                                } else {
                                    match position.checked_sub(1) {
                                        Some(new_position) => {
                                            position = new_position;
                                            continue
                                        },
                                        None => break,
                                    }
                                }
                            },
                            None => break,
                        }
                    }
                    stack.push(a);
                },
                (b'*', Ordinal, None) => {
                    let mut b = stack.pop_ordinal();
                    let mut a = stack.pop_ordinal();
                    a.append(&mut b);
                    stack.push(a);
                },
                (b':', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    if b.is_empty() {
                        stack.extend(RepeatFor::new(String::new(), a.len().saturating_add(1)));
                        continue 'instruction_iteration
                    }
                    let mut occurrence_count = 0;
                    let mut iter = a.iter().enumerate();
                    while iter.len() > 0 {
                        if iter.by_ref().map(|(_, c)| c).take(b.len()).eq(b.iter()) {
                            occurrence_count += 1;
                        }
                    }
                    stack.extend(RepeatFor::new(b, occurrence_count));
                },
                (b'%', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    if b.is_empty() {
                        stack.reserve(a.len().saturating_add(1));
                        stack.extend(a.iter().copied());
                        stack.push(String::new());
                        continue 'instruction_iteration
                    }
                    let mut iter = a.iter().enumerate();
                    let mut start = 0;
                    let mut end;
                    loop {
                        match iter.find(|(_, c)| **c == b[0]) {
                            Some((index, _)) => {
                                end = index;
                                if iter.by_ref().map(|(_, c)| c).take(b.len() - 1).ne(&b[1..]) {
                                    continue
                                }
                                stack.push(a[start..end].to_owned());
                                start = index + b.len();
                            },
                            None => {
                                let mut a = a;
                                a.splice(..start, String::new());
                                stack.push(a);
                                break
                            },
                        }
                    }
                },
                (b'E', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    let output = if y >= 0 {
                        match (u32::try_from(&y), i8::try_from(&x)) {
                            (Ok(exponent), _) => rug::ops::Pow::pow(x, exponent),
                            (Err(_), Ok(0))
                            | (Err(_), Ok(1))
                            => x,
                            (Err(_), Ok(-1)) => if y.is_odd() { x } else { x.abs() },
                            (Err(_), _) => {
                                dbg!(instruction_char, mode, x, y);
                                panic!("power instruction could not handle exponent")
                            },
                        }
                    } else {
                        let neg_y = -y;
                        match (u32::try_from(&neg_y), i8::try_from(&x)) {
                            (Ok(degree), _) if x < 0 => -(-x).root(degree),
                            (Ok(degree), _) => x.root(degree),
                            (Err(_), Ok(-1))
                            | (Err(_), Ok(0))
                            | (Err(_), Ok(1))
                            => x,
                            (Err(_), _) => {
                                let y = -neg_y;
                                dbg!(instruction_char, mode, x, y);
                                panic!("power instruction could not handle exponent")
                            },
                        }
                    };
                    stack.push(output);
                },
                (b'E', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    let mut output = String::new();
                    let mut iter = a.into_iter();
                    if let Some(char_) = iter.next() {
                        output.push(char_);
                    }
                    for char_ in iter {
                        output.extend(&b);
                        output.push(char_);
                    }
                    stack.push(output);
                },
                (b'H', Cardinal, None) => { let n = stack.pop_cardinal(); stack.push(n.abs()); },
                (b'H', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    let is_space = |&c| match c { '\t' | '\n' | ' ' => true, _ => false };
                    let end = s.iter()
                               .enumerate()
                               .rfind(|(_, c)| !is_space(c))
                               .map_or(0, |(i, _)| i);
                    let start = s.iter()
                                 .position(|c| !is_space(c))
                                 .unwrap_or(s.len());
                    s.splice(end+1.., String::new());
                    s.splice(0..start, String::new());
                },
                (b'R', Cardinal, None) => { let n = stack.pop_cardinal(); stack.push(-n); },
                (b'R', Ordinal, None) => { let mut s = stack.pop_ordinal(); s.reverse(); stack.push(s); },
                (b'h', Cardinal, None) => { let n = stack.pop_cardinal(); stack.push(n + 1); },
                (b'h', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    stack.push(if !s.is_empty() { Some(s.remove(0)) } else { None });
                    stack.push(s);
                },
                (b't', Cardinal, None) => { let n = stack.pop_cardinal(); stack.push(n - 1); },
                (b't', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    let tail = s.pop();
                    stack.push(s);
                    stack.push(tail);
                },
                (b'm', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    if y == 0 {
                        match options.divide_by_zero {
                            DivideByZeroAction::Panic => {
                                dbg!(instruction_char, mode, x, y);
                                panic!("arithmetic instruction resulted in division by zero")
                            },
                            DivideByZeroAction::Exit => break 'main_loop,
                        }
                    }
                    stack.push(rug::ops::DivRounding::div_floor(x, &y) * y);
                },
                (b'm', Ordinal, None) => {
                    let mut b = stack.pop_ordinal();
                    let mut a = stack.pop_ordinal();
                    let new_len = min(a.len(), b.len());
                    a.truncate(new_len);
                    b.truncate(new_len);
                    stack.push(a);
                    stack.push(b);
                },
                (b'n', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    n.assign(if n == 0 { 1 } else { 0 });
                    stack.push(n);
                },
                (b'n', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    if s.is_empty() {
                        s.splice(.., "Jabberwocky".chars());
                    } else {
                        s.clear();
                    }
                    stack.push(s);
                },
                (b'Y', Cardinal, None)
                | (b'Z', Cardinal, None)
                => {
                    fn pack_nonnegative(n: Int) -> Int {
                        if n >= 0 { n * 2 } else { -n * 2 - 1 }
                    }
                    fn unpack_nonnegative(n: Int) -> Int {
                        if n.is_even() { n / 2 } else { -n / 2 - 1 }
                    }
                    match instruction {
                        b'Z' => {
                            let y = pack_nonnegative(stack.pop_cardinal());
                            let x = pack_nonnegative(stack.pop_cardinal());
                            let n = Int::from(&x + &y) * (x + &y + 1) / 2 + y;
                            stack.push(unpack_nonnegative(n));
                        },
                        b'Y' => {
                            let n = pack_nonnegative(stack.pop_cardinal());
                            let z = n;
                            let w = {
                                let radical: Int = Int::from(&z * 8) + 1;
                                let w: Int = (radical.root(2) - 1) / 2;
                                w
                            };
                            let t = (rug::ops::Pow::pow(w.clone(), 2) + &w) / 2;
                            let y = z - t;
                            let x = w - &y;
                            stack.push(unpack_nonnegative(x));
                            stack.push(unpack_nonnegative(y));
                        },
                        _ => unreachable!(),
                    }
                },
                (b'Y', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let mut a = String::with_capacity(s.len() / 2 + s.len() % 2);
                    let mut b = String::with_capacity(s.len() / 2);
                    let mut iter = s.into_iter();
                    loop {
                        match iter.next() {
                            Some(char_) => a.push(char_),
                            None => break,
                        }
                        match iter.next() {
                            Some(char_) => b.push(char_),
                            None => break,
                        }
                    }
                    stack.push(a);
                    stack.push(b);
                },
                (b'Z', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    let mut s = String::with_capacity(a.len() + b.len());
                    let mut b_iter = b.into_iter();
                    let mut a_iter = a.into_iter();
                    let shared_len = min(a_iter.len(), b_iter.len());
                    for (a_char, b_char) in a_iter.by_ref().zip(b_iter.by_ref()).take(shared_len) {
                        s.push(a_char);
                        s.push(b_char);
                    }
                    s.extend(a_iter);
                    s.extend(b_iter);
                    stack.push(s);
                },

                // Bitwise arithmetic, multiset operations, and character transformation
                (b'A', Cardinal, None)
                | (b'V', Cardinal, None)
                | (b'X', Cardinal, None)
                => {
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    let output = match instruction {
                        b'A' => x & y,
                        b'V' => x | y,
                        b'X' => x ^ y,
                        _ => unreachable!(),
                    };
                    stack.push(output);
                },
                (b'N', Cardinal, None) => { let n = stack.pop_cardinal(); stack.push(!n); },
                (b'A', Ordinal, None)
                | (b'V', Ordinal, None)
                | (b'X', Ordinal, None)
                | (b'N', Ordinal, None)
                => {
                    fn decrement(n: &mut usize) -> bool {
                        if *n > 0 { *n -= 1; true } else { false }
                    }
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    let mut occurrences = {
                        let mut occurrences = HashMap::new();
                        for &char_ in &a {
                            occurrences.entry(char_).or_insert((0, 0)).0 += 1;
                        }
                        for &char_ in &b {
                            occurrences.entry(char_).or_insert((0, 0)).1 += 1;
                        }
                        occurrences
                    };
                    let output = if options.ordered_sets {
                        let mut filter_occurrences = |f_a: fn(&mut _) -> _, f_b: fn(&mut _) -> _| {
                            let mut output = String::new();
                            output.extend(a.iter().filter(|&&c| occurrences.get_mut(&c).map_or(false, f_a)).copied());
                            output.extend(b.iter().filter(|&&c| occurrences.get_mut(&c).map_or(false, f_b)).copied());
                            output
                        };
                        match instruction {
                            b'A' => filter_occurrences(|(_, n)| decrement(n), |_| false),
                            b'V' => filter_occurrences(|_| true, |(n, _)| !decrement(n)),
                            b'X' => filter_occurrences(|(_, n)| !decrement(n), |(n, _)| !decrement(n)),
                            b'N' => filter_occurrences(|(_, n)| !decrement(n), |_| false),
                            _ => unreachable!(),
                        }
                    } else {
                        let combine_occurrences = |f: fn(_, _) -> _| occurrences.iter().flat_map(|(&c, &(n_a, n_b))| std::iter::repeat(c).take(f(n_a, n_b))).collect();
                        match instruction {
                            b'A' => combine_occurrences(min),
                            b'V' => combine_occurrences(max),
                            b'X' => combine_occurrences(|m, n| max(m, n) - min(m, n)),
                            b'N' => combine_occurrences(|n_a, n_b| n_a.saturating_sub(n_b)),
                            _ => unreachable!(),
                        }
                    };
                    stack.push(output);
                },
                (b'y', Cardinal, None) => {
                    let z = stack.pop_cardinal();
                    let y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    let output = (&x & y) | (!x & z);
                    stack.push(output);
                },
                (b'y', Ordinal, None) => {
                    let c = stack.pop_ordinal();
                    let b = stack.pop_ordinal();
                    let mut a = stack.pop_ordinal();
                    let mut output;
                    if !c.is_empty() {
                        let mut positions = HashMap::new();
                        for char_ in &mut a {
                            let position = positions.entry(*char_).or_insert((0, 0));
                            let maybe_offset = b.iter()
                                                .skip(position.0)
                                                .chain(&b)
                                                .take(b.len())
                                                .position(|&x| x == *char_);
                            if let Some(offset) = maybe_offset {
                                position.0 = (position.0 + offset) % b.len();
                                position.1 = (position.1 + offset) % c.len();
                                *char_ = c[position.1];
                                position.0 += 1;
                                position.1 += 1;
                            }
                        }
                        output = a;
                    } else {
                        let mut b = b;
                        let skip_chars: HashSet<_> = b.drain(..).collect();
                        // We can just reuse this allocation.
                        output = b;
                        output.extend(a.iter().copied().filter(|x| !skip_chars.contains(x)));
                    }
                    stack.push(output);
                },
                (b'l', Cardinal, None)
                | (b'u', Cardinal, None)
                => {
                    let mut n = stack.pop_cardinal();
                    let i;
                    if n >= 0 {
                        i = n.significant_bits().saturating_sub(1);
                    } else {
                        rug::ops::NotAssign::not_assign(&mut n);
                        i = n.significant_bits().saturating_sub(1);
                        rug::ops::NotAssign::not_assign(&mut n);
                    };
                    let pattern: Int = (Int::from(1) << i) - 1;
                    match instruction {
                        b'l' => n &= !pattern,
                        b'u' => n |= pattern,
                        _ => unreachable!(),
                    }
                    stack.push(n);
                },
                (b'l', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    for char_ in &mut s {
                        let mut iter = char_.to_lowercase();
                        if char_.is_uppercase() && iter.len() == 1 {
                            *char_ = iter.next().unwrap();
                        }
                    }
                    stack.push(s);
                },
                (b'u', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    for char_ in &mut s {
                        let mut iter = char_.to_uppercase();
                        if char_.is_lowercase() && iter.len() == 1 {
                            *char_ = iter.next().unwrap();
                        }
                    }
                    stack.push(s);
                },

                // Number theory and advanced string operations
                (b'B', Cardinal, None) => {
                    let n = stack.pop_cardinal();
                    let increment = if n >= 0 { 1 } else { -1 };
                    let mut potential_divisor = Int::from(increment);
                    while *potential_divisor.as_abs() <= *n.as_abs() {
                        if n.is_divisible(&potential_divisor) {
                            stack.push(potential_divisor.clone());
                        }
                        potential_divisor += increment;
                    }
                },
                (b'B', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    for len in 1..=s.len() {
                        for start in 0..s.len() {
                            if let Some(slice) = s.get(start..start+len) {
                                stack.push(slice.to_owned())
                            } else {
                                break
                            }
                        }
                    }
                },
                (b'D', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    if n == 0 {
                        stack.push(n);
                        continue 'instruction_iteration
                    }
                    let mut p = Int::from(2);
                    let signum = if n > 0 { 1 } else { -1 };
                    let mut output = Int::from(signum);
                    while n != signum {
                        if n.remove_factor_mut(&p) > 0 {
                            output *= &p;
                        }
                        p.next_prime_mut();
                    }
                    stack.push(output);
                },
                (b'D', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let mut seen_chars = HashSet::new();
                    let mut output = String::new();
                    for char_ in s {
                        if seen_chars.replace(char_).is_none() {
                            output.push(char_);
                        }
                    }
                    stack.push(output);
                },
                (b'F', Cardinal, None) => {
                    let mut y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    if !(y != 0 && x.is_divisible(&y)) {
                        y.assign(0);
                    }
                    stack.push(y);
                },
                (b'F', Ordinal, None) => {
                    let mut b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    if b.is_empty() {
                        stack.push(b);
                        continue 'instruction_iteration
                    }
                    let mut iter = a.iter();
                    while iter.len() > 0 {
                        if iter.by_ref().take(b.len()).eq(&b) {
                            stack.push(b);
                            continue 'instruction_iteration
                        }
                    }
                    b.clear();
                    stack.push(b);
                },
                (b'G', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let mut x = stack.pop_cardinal();
                    x.gcd_mut(&y);
                    stack.push(x);
                },
                (b'G', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    for len in (1..=a.len()).rev() {
                        let mut common_substrings = Stack::new();
                        'windows: for substring in a.windows(len) {
                            for stack_item in common_substrings.iter() {
                                match stack_item {
                                    StackItem::Ordinal(existing) if *existing == substring => continue 'windows,
                                    _ => {},
                                }
                            }
                            let mut iter = b.iter();
                            while iter.len() > 0 {
                                if iter.by_ref().take(len).eq(substring) {
                                    common_substrings.push(substring.to_owned());
                                    break
                                }
                            }
                        }
                        if !common_substrings.is_empty() {
                            stack.append(&mut common_substrings);
                            continue 'instruction_iteration
                        }
                    }
                    stack.push(String::new());
                },
                (b'L', Cardinal, None) => {
                    let y = stack.pop_cardinal();
                    let mut x = stack.pop_cardinal();
                    x.lcm_mut(&y);
                    stack.push(x);
                },
                (b'L', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let mut a = stack.pop_ordinal();
                    for start in a.len().saturating_sub(b.len()).. {
                        let (overlap, b_suffix) = b.split_at(a.len() - start);
                        if &a[start..] == overlap {
                            a.extend(b_suffix);
                            stack.push(a);
                            break
                        }
                    }
                },
                (b'S', Cardinal, None) => {
                    let z = stack.pop_cardinal();
                    let y = stack.pop_cardinal();
                    let mut x = stack.pop_cardinal();
                    if x == 0 {
                        stack.push(x);
                    } else if y == 1 || y == -1 {
                        if z == y {
                            stack.push(x);
                        } else if z == 0 {
                            x.assign(0);
                            stack.push(x);
                        } else {
                            match options.replace_divisor_one {
                                ReplaceDivisorOneAction::Hang => loop {
                                    std::thread::sleep(std::time::Duration::from_secs(u64::MAX));
                                },
                                ReplaceDivisorOneAction::Panic => {
                                    dbg!(instruction_char, mode, x, y, z);
                                    panic!("replace-divisor instruction resulted in infinite loop")
                                },
                                ReplaceDivisorOneAction::PushX => {
                                    stack.push(x);
                                },
                            }
                        }
                    } else {
                        let count = x.remove_factor_mut(&y);
                        x *= rug::ops::Pow::pow(z, count);
                        stack.push(x);
                    }
                },
                (b'S', Ordinal, None) => {
                    let c = stack.pop_ordinal();
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    let mut output = String::new();
                    if b.is_empty() {
                        output.extend(&c);
                        for char_ in a {
                            output.push(char_);
                            output.extend(&c);
                        }
                        stack.push(output);
                        continue 'instruction_iteration
                    }
                    let mut iter = a.iter().enumerate();
                    let mut start = 0;
                    let mut end;
                    loop {
                        match iter.find(|(_, x)| **x == b[0]) {
                            Some((index, _)) => {
                                end = index;
                                if iter.by_ref().map(|(_, x)| x).take(b.len() - 1).ne(&b[1..]) {
                                    continue
                                }
                                output.extend(&a[start..end]);
                                output.extend(&c);
                                start = index + b.len();
                            },
                            None => {
                                if !output.is_empty() || start > 0 {
                                    output.extend(&a[start..]);
                                    stack.push(output);
                                } else {
                                    stack.push(a);
                                }
                                break
                            },
                        }
                    }
                },
                (b'c', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    if n == 0 {
                        stack.push(n);
                        continue 'instruction_iteration
                    }
                    if n < 0 {
                        stack.push(-1);
                        n = -n;
                    }
                    let mut p = Int::from(2);
                    while n > 1 {
                        let count = n.remove_factor_mut(&p);
                        for _ in 0..count {
                            stack.push(p.clone());
                        }
                        p.next_prime_mut();
                    }
                },
                (b'c', Ordinal, None) => { for char_ in stack.pop_ordinal() { stack.push(char_) } },
                (b'f', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    if n == 0 {
                        stack.push(n);
                        stack.push(Int::from(1));
                        continue 'instruction_iteration
                    }
                    if n < 0 {
                        stack.push(Int::from(-1));
                        stack.push(Int::from(1));
                        n = -n;
                    }
                    let mut p = Int::from(2);
                    while n > 1 {
                        let count = n.remove_factor_mut(&p);
                        if count > 0 {
                            stack.push(p.clone());
                            stack.push(Int::from(count));
                        }
                        p.next_prime_mut();
                    }
                },
                (b'f', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let mut iter = s.iter().copied();
                    let mut char_ = match iter.next() {
                        Some(char_) => char_,
                        None => continue 'instruction_iteration,
                    };
                    let mut count = 1;
                    loop {
                        let maybe_next_char = iter.next();
                        if maybe_next_char.map_or(false, |c| c == char_) {
                            count += 1;
                        } else {
                            stack.push([char_].repeat(count));
                            count = 1;
                        }
                        match maybe_next_char {
                            Some(next_char) => char_ = next_char,
                            None => break,
                        }
                    }
                },
                (b'z', Cardinal, None) => {
                    let mut y = stack.pop_cardinal();
                    let mut x = stack.pop_cardinal();
                    if x == 0 {
                        stack.push(x);
                        continue 'instruction_iteration
                    }
                    let flip_sign;
                    if y < 0 {
                        y = -y;
                        flip_sign = true;
                    } else {
                        flip_sign = false;
                    }
                    let mut p = Int::from(2);
                    while p <= y && *x.as_abs() > 1 {
                        let count = x.remove_factor_mut(&p);
                        if count % 2 != 0 && flip_sign {
                            x = -x;
                        }
                        p.next_prime_mut();
                    }
                    stack.push(x);
                },
                (b'z', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let mut a = stack.pop_ordinal();
                    let mut iter = a.iter().enumerate();
                    while iter.len() > 0 {
                        if iter.by_ref().map(|(_, c)| c).take(b.len()).eq(&b) {
                            if let Some((end, _)) = iter.next() {
                                a.splice(..end, String::new());
                            } else {
                                a.clear();
                            }
                            break
                        }
                    }
                    stack.push(a);
                },

                // Combinatorics
                (b'C', Cardinal, None) => {
                    let mut k = stack.pop_cardinal();
                    let n = stack.pop_cardinal();
                    let mut scratch = Int::new();
                    if n > 0 && &k > { scratch.assign(&n / 2); &scratch } {
                        scratch.assign(&n - &k);
                        swap(&mut k, &mut scratch);
                    }
                    let mut output;
                    match k.cmp0() {
                        // For these two, we don't need `scratch` again, so steal its allocation.
                        Ordering::Less => {
                            output = scratch;
                            output.assign(0);
                        },
                        Ordering::Equal => {
                            output = scratch;
                            output.assign(1);
                        },
                        Ordering::Greater => {
                            let mut end;
                            output = Int::from(1);
                            scratch.assign(&n);
                            end = n;
                            end -= &k;
                            end += 1;
                            while scratch >= end {
                                output *= &scratch;
                                scratch -= 1;
                            }
                            scratch.assign(&k);
                            while scratch > 1 {
                                output /= &scratch;
                                scratch -= 1;
                            }
                        },
                    }
                    stack.push(output);
                },
                (b'C', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let mut t = Vec::with_capacity(s.len());
                    for k in 0..=s.len() {
                        t.clear();
                        t.extend(0..k);
                        loop {
                            let subsequence: String = t.iter().copied().map(|i| s[i]).collect();
                            stack.push(subsequence);
                            let reset_count = t.iter_mut()
                                               .rev()
                                               .zip((0..s.len()).rev())
                                               .take_while(|(i, j)| *i == j)
                                               .count();
                            t.truncate(t.len().saturating_sub(reset_count));
                            if let Some(i) = t.last_mut() {
                                *i += 1;
                                let reset_indices = (*i+1..).take(k.saturating_sub(t.len()));
                                t.extend(reset_indices);
                            } else {
                                break
                            }
                        }
                    }
                },
                (b'P', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    let negate_output;
                    if n >= 0 {
                        negate_output = false;
                    } else {
                        negate_output = n.is_odd();
                        n = -n;
                    }
                    if let Ok(i) = u32::try_from(&n) {
                        n.assign(Int::factorial(i));
                    } else {
                        let mut output = Int::from(1);
                        while n > 1 {
                            output *= &n;
                        }
                        n = output;
                    }
                    if negate_output {
                        n = -n;
                    }
                    stack.push(n);
                },
                (b'P', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    let mut t: Vec<_> = (0..s.len()).collect();
                    loop {
                        stack.push(s.clone());
                        let maybe_k = (0..t.len().saturating_sub(1))
                                         .rev()
                                         .filter(|&k| t[k] < t[k + 1])
                                         .next();
                        let k = match maybe_k {
                            Some(k) => k,
                            None => break,
                        };
                        let l = (k.saturating_add(1)..t.len())
                                                     .rev()
                                                     .filter(|&l| t[l] > t[k])
                                                     .next()
                                                     .unwrap();
                        t.swap(k, l);
                        s.swap(k, l);
                        t[k+1..].reverse();
                        s[k+1..].reverse();
                    }
                },

                // Order, entorpy, and time
                (b'T', Cardinal, None) => {
                    let n = stack.pop_cardinal();
                    if n >= 0 {
                        // If `n` overflows `u64`, we're being asked to sleep for more than half a
                        // billion years. Just saturate.
                        let n_u64 = u64::try_from(n).unwrap_or(u64::MAX);
                        let duration = std::time::Duration::from_millis(n_u64);
                        std::thread::sleep(duration);
                    }
                },
                (b'T', Ordinal, None) => {
                    let now = time::OffsetDateTime::now_local();
                    let mut buffer = now.format("%Y-%m-%dT%H:%M:%S");
                    write!(
                        buffer,
                        ".{:03}{:+03}:{:02}",
                        now.millisecond(),
                        now.offset().as_hours(),
                        now.offset().as_minutes().abs() % 60,
                    ).unwrap();
                    // Yes, we mismatch byte counts and char counts, but it's okay because the
                    // string is all ASCII.
                    let mut string = String::with_capacity(buffer.len());
                    string.extend(buffer.chars());
                    stack.push(string);
                },
                (b'U', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    if n == 0 {
                        stack.push(n);
                        continue 'instruction_iteration
                    }
                    let output_negative;
                    if n >= 0 {
                        output_negative = false;
                    } else {
                        n = -n;
                        output_negative = true;
                    }
                    let mut output = Int::with_capacity(n.significant_bits() as usize);
                    let mut buffer = vec![0; n.significant_digits::<u64>()];
                    loop {
                        rng.fill(&mut *buffer);
                        let last = buffer.last_mut().unwrap();
                        *last &= u64::MAX >> ((64 - n.significant_bits() % 64) % 64);
                        output.assign_digits(&buffer, rug::integer::Order::Lsf);
                        if output < n {
                            break
                        }
                    }
                    if output_negative {
                        output = -output;
                    }
                    stack.push(output);
                },
                (b'U', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    if let Some(&char_) = rand::seq::SliceRandom::choose(&*s, &mut rng) {
                        stack.push(char_);
                    } else {
                        stack.push(s);
                    }
                },
                (b'b', Cardinal, None) => {
                    let mut y = stack.pop_cardinal();
                    let mut x = stack.pop_cardinal();
                    if rng.gen() {
                        swap(&mut x, &mut y);
                    }
                    stack.push(x);
                    stack.push(y);
                },
                (b'b', Ordinal, None) => {
                    let mut s = stack.pop_ordinal();
                    rand::seq::SliceRandom::shuffle(&mut *s, &mut rng);
                    stack.push(s);
                },
                (b'r', Cardinal, None) => {
                    let mut n = stack.pop_cardinal();
                    if n >= 0 {
                        let mut i = Int::with_capacity(n.significant_bits() as usize);
                        i.assign(0);
                        while i < n {
                            stack.push(i.clone());
                            i += 1;
                        }
                        stack.push(n);
                    } else {
                        n = -n;
                        while n > 0 {
                            stack.push(n.clone());
                            n -= 1;
                        }
                        stack.push(n);
                    }
                },
                (b'r', Ordinal, None) => {
                    let s = stack.pop_ordinal();
                    let mut output = String::new();
                    let mut iter = s.iter();
                    let mut last_char;
                    if let Some(&char_) = iter.next() {
                        output.push(char_);
                        last_char = char_;
                    } else {
                        stack.push(s);
                        continue 'instruction_iteration
                    }
                    while let Some(&char_) = iter.next() {
                        match char_.cmp(&last_char) {
                            Ordering::Greater => {
                                let range = u32::from(last_char)..=u32::from(char_);
                                output.extend(range.skip(1).filter_map(|i| char::try_from(i).ok()));
                            },
                            Ordering::Equal => continue,
                            Ordering::Less => {
                                let range = u32::from(char_)..=u32::from(last_char);
                                output.extend(range.rev().skip(1).filter_map(|i| char::try_from(i).ok()));
                            },
                        }
                        last_char = char_;
                    }
                    stack.push(output);
                },
                (b's', Cardinal, None) => {
                    let mut y = stack.pop_cardinal();
                    let mut x = stack.pop_cardinal();
                    if x > y {
                        swap(&mut x, &mut y);
                    }
                    stack.push(x);
                    stack.push(y);
                },
                (b's', Ordinal, None) => { let mut s = stack.pop_ordinal(); s.sort_unstable(); stack.push(s); },
                (b'x', Cardinal, None) => {
                    let mut y = stack.pop_cardinal();
                    let x = stack.pop_cardinal();
                    let mut output;
                    if y >= 0 {
                        let fallible_i = u32::try_from(&y);
                        output = y;
                        match fallible_i {
                            Ok(i) => output.assign(x.get_bit(i)),
                            Err(_) => output.assign(0),
                        }
                    } else {
                        y = -y;
                        let fallible_i = u32::try_from(&y);
                        output = y;
                        match fallible_i.ok().and_then(|i| x.significant_bits().checked_sub(i)) {
                            Some(i) => output.assign(x.get_bit(i)),
                            None => output.assign(0),
                        }
                    }
                    stack.push(output);
                },
                (b'x', Ordinal, None) => {
                    let b = stack.pop_ordinal();
                    let a = stack.pop_ordinal();
                    let mut a_filled: Vec<_> = {
                        let len_difference = b.len().saturating_sub(a.len());
                        a.iter()
                         .copied()
                         .map(Some)
                         .chain(std::iter::repeat(None).take(len_difference))
                         .collect()
                    };
                    let mut indices: Vec<_> = (0..b.len()).collect();
                    indices.sort_by_key(|&i| b[i]);
                    let mut output = String::with_capacity(a.len());
                    for index in indices {
                        if let Some(char_) = a_filled.get_mut(index).and_then(Option::take) {
                            output.push(char_);
                        }
                    }
                    output.extend(a.get(b.len()..).unwrap_or(&[]));
                    stack.push(output);
                },

                (b' ', _, None)
                | (0x00..=0x1f, _, None)
                | (0x7f..=0xff, _, None)
                => noiter {},
            }
        )
    }
}

fn args() -> (std::ffi::OsString, Options, std::vec::IntoIter<String>) {
    use clap::{App, AppSettings, Arg};

    let args = App::new("alice.rs")
                   .version(clap::crate_version!())
                   .about("A feature-rich, two-dimensional, recreational programming language, with mirrors. Unofficial Rust implementation.")
                   .setting(AppSettings::TrailingVarArg)
                   .setting(AppSettings::AllArgsOverrideSelf)
                   .setting(AppSettings::UnifiedHelpMessage)
                   .version_short("\0")
                   .help_short("\0")
                   .arg(Arg::with_name("source-file")
                            .required(true)
                            .help("The path to the Alice program to run"))
                   .arg(Arg::with_name("program-args")
                            .multiple(true)
                            .help("Arguments to pass through to the program"))
                   .arg(Arg::with_name("shared-tape-head")
                            .long("shared-tape-head")
                            .help("Use a single tape head for both modes"))
                   .arg(Arg::with_name("unordered-sets")
                            .long("unordered-sets")
                            .help("Unspecify the order of the output of multiset operations"))
                   .arg(Arg::with_name("ignore-trailing-newline")
                            .long("ignore-trailing-newline")
                            .help("If the program has a trailing newline, ignore it"))
                   .arg(Arg::with_name("divide-by-zero")
                            .long("divide-by-zero")
                            .takes_value(true)
                            .possible_values(&["panic", "exit"])
                            .value_name("action")
                            .help("What to do on division by zero"))
                   .arg(Arg::with_name("stuck")
                            .long("stuck")
                            .takes_value(true)
                            .possible_values(&["hang", "panic"])
                            .value_name("action")
                            .help("What to do if stuck execution is detected"))
                   .arg(Arg::with_name("replace-divisor-one")
                            .long("replace-divisor-one")
                            .takes_value(true)
                            .possible_values(&["hang", "panic", "push-x"])
                            .value_name("action")
                            .help("The last fallback for the replace-divisors instruction"))
                   .get_matches();
    let program_path = args.value_of_os("source-file").unwrap().to_owned();
    let program_args: Vec<_> = args.values_of_os("program-args")
                                   .into_iter()
                                   .flatten()
                                   .map(string_from_os_str_skipping)
                                   .collect();
    let separate_tape_heads = !args.is_present("shared-tape-head");
    let ordered_sets = !args.is_present("unordered-sets");
    let ignore_trailing_newline = args.is_present("ignore-trailing-newline");
    let divide_by_zero = match args.value_of("divide-by-zero") {
        Some("panic") | None => DivideByZeroAction::Panic,
        Some("exit") => DivideByZeroAction::Exit,
        _ => unreachable!(),
    };
    let stuck = match args.value_of("stuck") {
        Some("hang") | None => StuckAction::Hang,
        Some("panic") => StuckAction::Panic,
        _ => unreachable!(),
    };
    let replace_divisor_one = match args.value_of("replace-divisor-one") {
        Some("hang") | None => ReplaceDivisorOneAction::Hang,
        Some("panic") => ReplaceDivisorOneAction::Panic,
        Some("push-x") => ReplaceDivisorOneAction::PushX,
        _ => unreachable!(),
    };
    let options = Options {
        separate_tape_heads,
        ordered_sets,
        ignore_trailing_newline,
        divide_by_zero,
        stuck,
        replace_divisor_one,
    };
    (program_path, options, program_args.into_iter())
}

#[derive(Debug, Clone)]
struct Options {
    separate_tape_heads: bool,
    ordered_sets: bool,
    ignore_trailing_newline: bool,
    divide_by_zero: DivideByZeroAction,
    stuck: StuckAction,
    replace_divisor_one: ReplaceDivisorOneAction,
}

#[derive(Debug, Clone, Copy)]
enum DivideByZeroAction {
    Panic,
    Exit,
}

#[derive(Debug, Clone, Copy)]
pub(crate) enum StuckAction {
    Hang,
    Panic,
}

#[derive(Debug, Clone, Copy)]
enum ReplaceDivisorOneAction {
    Hang,
    Panic,
    PushX,
}

#[derive(Debug, Clone)]
pub(crate) struct RepeatFor<T: Clone> {
    item: Option<T>,
    count: usize,
}

impl<T: Clone> RepeatFor<T> {
    pub(crate) fn new(item: T, count: usize) -> Self {
        RepeatFor { item: Some(item), count }
    }
}

impl<T: Clone> Iterator for RepeatFor<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        let count = self.count;
        self.count = self.count.saturating_sub(1);
        match count {
            0 => None,
            1 => self.item.take(),
            _ => self.item.as_ref().cloned(),
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
}

impl<T: Clone> std::iter::FusedIterator for RepeatFor<T> {}

impl<T: Clone> ExactSizeIterator for RepeatFor<T> {}

impl<T: Clone> DoubleEndedIterator for RepeatFor<T> {
    fn next_back(&mut self) -> Option<T> {
        self.next()
    }
}

#[cfg(unix)]
fn string_from_os_str_skipping(os_str: &std::ffi::OsStr) -> String {
    use std::os::unix::ffi::OsStrExt;

    std::io::Cursor::new(os_str.as_bytes()).read_chars_to_end().unwrap()
}

#[cfg(windows)]
fn string_from_os_str_skipping(os_str: &std::ffi::OsStr) -> String {
    use std::os::windows::ffi::OsStrExt;
    use encode_unicode::IterExt;

    os_str.encode_wide()
          .to_utf16chars()
          .filter_map(Result::ok)
          .map(|x| x.to_char())
          .collect()
}

fn char_try_from_int(int: &Int) -> Option<char> {
    u32::try_from(int).ok().and_then(|n| char::try_from(n).ok())
}
