use std::cmp::min;
use std::io::{BufRead, Error};

pub trait ReadExt: BufRead {
    fn read_char_skipping(&mut self) -> Result<Option<char>, Error> {
        read_chars::<SingleChar, _>(self)
    }

    fn read_chars_to_end(&mut self) -> Result<Vec<char>, Error> {
        read_chars::<UntilEof, _>(self)
    }

    fn read_chars_line(&mut self) -> Result<Vec<char>, Error> {
        read_chars::<UntilNewline, _>(self)
    }
}

impl<R: BufRead> ReadExt for R {}

fn read_chars<C: ReadConfig, R: BufRead>(mut reader: R) -> Result<C::Output, Error> {
    let mut output = C::empty();
    loop {
        let buffer = reader.fill_buf()?;
        if buffer.is_empty() {
            return Ok(output)
        }
        let slice = if let Some(limit) = C::BUFFER_LIMIT {
            &buffer[..min(buffer.len(), limit)]
        } else {
            buffer
        };
        let string = match std::str::from_utf8(slice) {
            Ok(string) => string,
            Err(error) if error.valid_up_to() > 0 => {
                std::str::from_utf8(&slice[..error.valid_up_to()]).unwrap()
            },
            Err(error) => {
                reader.consume(error.error_len().unwrap());
                continue
            },
        };
        let (consumed_len, is_done) = C::extend_output(&mut output, string);
        reader.consume(consumed_len);
        if is_done {
            return Ok(output)
        }
    }
}

trait ReadConfig {
    type Output;
    const BUFFER_LIMIT: Option<usize>;

    fn empty() -> Self::Output;

    fn extend_output(output: &mut Self::Output, input: &str) -> (usize, bool);
}

struct SingleChar;

impl ReadConfig for SingleChar {
    type Output = Option<char>;
    const BUFFER_LIMIT: Option<usize> = Some(64);

    fn empty() -> Option<char> {
        None
    }

    fn extend_output(output: &mut Option<char>, input: &str) -> (usize, bool) {
        match output {
            None => match input.chars().next() {
                Some(char_) => {
                    *output = Some(char_);
                    (char_.len_utf8(), true)
                },
                None => (0, false),
            },
            Some(_) => (0, true),
        }
    }
}

struct UntilNewline;

impl ReadConfig for UntilNewline {
    type Output = Vec<char>;
    const BUFFER_LIMIT: Option<usize> = None;

    fn empty() -> Vec<char> {
        Vec::new()
    }

    fn extend_output(output: &mut Vec<char>, input: &str) -> (usize, bool) {
        let mut consumed_len = 0;
        for char_ in input.chars() {
            consumed_len += char_.len_utf8();
            if char_ == '\n' {
                return (consumed_len, true)
            }
            output.push(char_);
        }
        (consumed_len, false)
    }
}

struct UntilEof;

impl ReadConfig for UntilEof {
    type Output = Vec<char>;
    const BUFFER_LIMIT: Option<usize> = None;

    fn empty() -> Vec<char> {
        Vec::new()
    }

    fn extend_output(output: &mut Vec<char>, input: &str) -> (usize, bool) {
        let mut consumed_len = 0;
        for char_ in input.chars() {
            consumed_len += char_.len_utf8();
            output.push(char_);
        }
        (consumed_len, false)
    }
}
